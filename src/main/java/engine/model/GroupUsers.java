package engine.model;

public class GroupUsers {
	private int id_GroupUsers;
    private int id_Group;
    private int id_User;
    
    public GroupUsers(){
    	super();
    }
    
    public GroupUsers(int id_Group, int id_User){
        this.id_Group = id_Group;
        this.id_User = id_User;
    }
    
	public int getId_User() {
		return id_User;
	}

	public void setId_User(int id_User) {
		this.id_User = id_User;
	}

	public int getId_GroupUsers() {
		return id_GroupUsers;
	}
	
	public void setId_GroupUsers(int id_GroupUsers) {
		this.id_GroupUsers = id_GroupUsers;
	}
	
	public int getId_Group() {
		return id_Group;
	}
	
	public void setId_Group(int id_Group) {
		this.id_Group = id_Group;
	}
}
