package engine.model;

import java.util.ArrayList;
import java.util.List;

public class Group {
	private int id_Group;
	private String groupName;
	private List<GroupUsers> usersList = new ArrayList<GroupUsers>();
	
	public Group(){
		super();
	}
	
	public Group(String group_Name){
		this.groupName = group_Name;
	}
	
	public List<GroupUsers> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<GroupUsers> usersList) {
		this.usersList = usersList;
	}

	public int getId_Group() {
		return id_Group;
	}

	public void setId_Group(int id_Group) {
		this.id_Group = id_Group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
