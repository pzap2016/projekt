package engine.model;
import java.util.ArrayList;
import java.util.List;


public class SolvedTests {
	private int id_SolvedTest;
    private String testName;
    private String userName;
    private int id_User;
    private int score;
    private String date;
    private List<SolvedTestsHistory> historyList = new ArrayList<SolvedTestsHistory>();

    public SolvedTests(){ super(); }

    public SolvedTests(int id_SolvedTest, String userName, String testName, int id_User, int score, String date) {
        this.id_SolvedTest = id_SolvedTest;
        this.userName = userName;
    	this.testName = testName;
        this.id_User = id_User;
        this.score = score;
        this.date = date;
    }

	public int getId_SolvedTest() {
		return id_SolvedTest;
	}

	public void setId_SolvedTest(int id_SolvedTest) {
		this.id_SolvedTest = id_SolvedTest;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getId_User() {
		return id_User;
	}

	public void setId_User(int id_User) {
		this.id_User = id_User;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<SolvedTestsHistory> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<SolvedTestsHistory> historyList) {
		this.historyList = historyList;
	}
}
