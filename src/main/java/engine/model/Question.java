package engine.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by draxeer on 2017-01-21.
 */
public class Question {
    private int id_Question;
    private int id_Test;
    private String question;
    private String img;
    private List<Answer> answerList = new ArrayList<Answer>();

    public Question(){super();}

    public Question(int id_Test, String question, String img) {
        this.id_Test = id_Test;
        this.question = question;
        this.img = img;
    }


    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    public int getId_Test() {
        return id_Test;
    }

    public void setId_Test(int id_Test) {
        this.id_Test = id_Test;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
