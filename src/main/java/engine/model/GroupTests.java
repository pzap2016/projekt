package engine.model;

public class GroupTests {
	private int id_GroupUsers;
    private int id_Group;
    private int id_Test;
    
    public GroupTests(){
    	super();
    }
    
    public GroupTests(int id_Group, int id_Test){
        this.id_Group = id_Group;
        this.id_Test = id_Test;
    }
    
	public int getId_Test() {
		return id_Test;
	}

	public void setId_Test(int id_Test) {
		this.id_Test = id_Test;
	}

	public int getId_GroupUsers() {
		return id_GroupUsers;
	}
	public void setId_GroupUsers(int id_GroupUsers) {
		this.id_GroupUsers = id_GroupUsers;
	}
	public int getId_Group() {
		return id_Group;
	}
	public void setId_Group(int id_Group) {
		this.id_Group = id_Group;
	}
}
