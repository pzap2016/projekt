package engine.model;

/**
 * Created by draxeer on 2017-01-22.
 */
public class Answer {
    private int id_Answer;
    private int id_Question;
    private String answer;
    private Boolean is_Correct;

    public Answer(){super();}
    public Answer(int id_Question, String answer, Boolean is_Correct) {
        this.id_Question = id_Question;
        this.answer = answer;
        this.is_Correct = is_Correct;
    }

    public int getId_Answer() {
        return id_Answer;
    }

    public void setId_Answer(int id_Answer) {
        this.id_Answer = id_Answer;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getIs_Correct() {
        return is_Correct;
    }

    public void setIs_Correct(Boolean is_Correct) {
        this.is_Correct = is_Correct;
    }
}
