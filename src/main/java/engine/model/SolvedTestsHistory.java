package engine.model;
import java.util.ArrayList;
import java.util.List;

public class SolvedTestsHistory {

    private int id_SolvedTest;
    private String questionName;
    private String answerName;

    public SolvedTestsHistory(){ super(); }

    public SolvedTestsHistory(int id_SolvedTest, String questionName, String answerName) {
        this.id_SolvedTest = id_SolvedTest;
        this.questionName = questionName;
        this.answerName = answerName;
    }

	public int getId_SolvedTest() {
		return id_SolvedTest;
	}

	public void setId_SolvedTest(int id_SolvedTest) {
		this.id_SolvedTest = id_SolvedTest;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getAnswerName() {
		return answerName;
	}

	public void setAnswerName(String answerName) {
		this.answerName = answerName;
	}
}
