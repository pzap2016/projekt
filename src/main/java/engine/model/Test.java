package engine.model;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by draxeer on 2017-01-21.
 */
public class Test {

    private int id_Test;
    private User author;
    private String testName;
    private Integer questions_per_test;
    private Integer time_per_test;
    private List<Question> questionList = new ArrayList<Question>();

    public Test(){ super(); }

    public Integer getQuestions_per_test() {
        return questions_per_test;
    }

    public void setQuestions_per_test(Integer questions_per_test) {
        this.questions_per_test = questions_per_test;
    }

    public Integer getTime_per_test() {
        return time_per_test;
    }

    public void setTime_per_test(Integer time_per_test) {
        this.time_per_test = time_per_test;
    }

    public Test(int id_Test, User user, String test_Name, int Questions_per_test, int Time_per_test) {
        this.id_Test = id_Test;
        this.author = user;
        this.testName = test_Name;
        this.questions_per_test = Questions_per_test;
        this.time_per_test = Time_per_test;
    }

    public User getAuthor() {
        return author;
    }
    public void setAuthor(User author) {
        this.author = author;
    }
    public List<Question> getQuestionList() { return questionList; }
    public void setQuestionList(List<Question> questionList) { this.questionList = questionList; }
    public int getId_Test() { return id_Test; }
    public void setId_Test(int id_Test) { this.id_Test = id_Test; }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
