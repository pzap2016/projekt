package engine.model;

/**
 * Created by draxeer on 2016-12-10.
 */
public class User {
    private int id_User;
    private int id_Role;
	private String first_name;
    private String last_name;
    private String passw;
    private int st_index;

    public int getId_Role() {
        return id_Role;
    }

    public void setId_Role(int id_Role) {
        this.id_Role = id_Role;
    }

    private String email;

    public User(String first_name, String last_name, String passw, int st_index, String email) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.passw = passw;
        this.st_index = st_index;
        this.email = email;
    }
    public User(){super();}

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public int getSt_index() {
        return st_index;
    }

    public void setSt_index(int st_index) {
        this.st_index = st_index;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
