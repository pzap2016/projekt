package engine.servlets;
import engine.controller.UserDAO;
import engine.model.Group;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Groups")
public class Groups extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin")) {
	        ArrayList<Group> list = UserDAO.getAllGroups();
	        request.setAttribute("groups", list);
	        request.getRequestDispatcher("ViewGroups.jsp").forward(request, response);
	    }
		else {
			response.sendRedirect("accessDeniedPage.jsp");
		}
    }
}
