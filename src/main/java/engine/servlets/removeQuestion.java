package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/removeQuestion")
public class removeQuestion extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int id = Integer.parseInt(request.getParameter("idQuestion"));
            int id2 = Integer.parseInt(request.getParameter("idtest"));
            response.setContentType("text/html;charset=UTF-8");
            if(TestDAO.removeQuestion(id)==1) {
                request.setAttribute("delP", "asd");
                response.sendRedirect("Tests/Details?id="+id2+"&succes=1");
            }
            else{
                response.sendRedirect("Tests/Details?id="+id2+"&succes=0");
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
