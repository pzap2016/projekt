package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.User;
import engine.model.Test;
import engine.model.Group;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/Editors")
public class TestEditors extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin")) {
			int id = Integer.parseInt(request.getParameter("id"));
			ArrayList<User> editorsList = UserDAO.getTestEditors(id);
			ArrayList<User> otherList = UserDAO.getOtherEditors(id);
			Test test = TestDAO.getTestById(id);
			request.setAttribute("editors", editorsList);
			request.setAttribute("otherUsers", otherList);
			request.setAttribute("test", test);
			request.getRequestDispatcher("/testEditors.jsp").forward(request, response);
		}
		else {
			response.sendRedirect("accessDeniedPage.jsp");
		}
    }
}