package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.SolvedTests;
import engine.model.Test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/History")
public class history extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	String role = (String) session.getAttribute("role");
    	
    	if (UserDAO.authorize(role, "Admin", "Edytor", "User")) {
        	int idUser = (Integer) session.getAttribute("idUser");
        	ArrayList<SolvedTests> history = TestDAO.getUserSolvedTests(idUser);
        	request.setAttribute("history", history);
            request.getRequestDispatcher("/history.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}