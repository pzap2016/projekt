package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.SolvedTests;
import engine.model.Test;
import engine.model.User;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/HistoryDetails")
public class historyDetails extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	String role = (String) session.getAttribute("role");
    	
    	if (UserDAO.authorize(role, "Admin", "Edytor", "User")) {
        	int idUser = 0;
        	if(request.getParameter("idUser") != null){
        		idUser = Integer.parseInt(request.getParameter("idUser"));
        	} else {
        		idUser = (Integer) session.getAttribute("idUser");
        	}
        	int idST = Integer.parseInt(request.getParameter("id"));
        	SolvedTests history = TestDAO.getSolvedTestDetails(idST);
        	User user = UserDAO.getUserData(idUser);
        	request.setAttribute("history", history);
        	request.setAttribute("user", user);
            request.getRequestDispatcher("/historyDetails.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}