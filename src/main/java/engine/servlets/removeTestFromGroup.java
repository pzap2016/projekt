package engine.servlets;
import engine.controller.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Groups/removeTestFromGroup")
public class removeTestFromGroup extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int idTest = Integer.parseInt(request.getParameter("idTest"));
            int idGroup = Integer.parseInt(request.getParameter("idGroup"));

            if(UserDAO.RemoveTestFromGroup(idTest, idGroup)==1) {
                response.sendRedirect("../Tests/groupTests?id="+idGroup);
            }
        }
            else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}