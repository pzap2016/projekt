package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Test;
import engine.model.User;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/CreateTest")
public class CreateTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
        	request.getRequestDispatcher("createTest.jsp").forward(request, response);
    	}
    	else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
    	String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
	        Test t = new Test();
	        t.setTestName(request.getParameter("test_name"));
	        t.setTime_per_test(Integer.parseInt(request.getParameter("time")));
	        t.setQuestions_per_test(Integer.parseInt(request.getParameter("number")));
	        User u = new User();
	        u = UserDAO.getUserByName(request.getParameter("username"));
	        t.setAuthor(u);
	        if(TestDAO.createTest(t)==1){
	            int i = TestDAO.getTestByName(t.getTestName());
	            if(i==-1){
	                request.setAttribute("error", EncodeToUTF("Błąd w trakcie dodawania testu"));
	                request.getRequestDispatcher("createTest.jsp").forward(request, response);
	            }
	            else{
	            	// dodaję edytora do testu
					if (UserDAO.authorize(userRole, "Edytor"))
						TestDAO.AddEditorToTest(u.getId_User(), i);

	                response.sendRedirect("Tests?succesA=1");
	            }
	        }
	        else{
	            request.setAttribute("error", EncodeToUTF("Błąd w trakcie dodawania testu"));
	            request.getRequestDispatcher("createTest.jsp").forward(request, response);
	    	}
	    }
	    else {
			response.sendRedirect("accessDeniedPage.jsp");
		}
    }

}
