package engine.servlets;
import engine.controller.UserDAO;
import engine.model.User;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Created by draxeer on 2016-12-17.
 */

@WebServlet(urlPatterns = "/accManager")
public class accManager extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
        	ArrayList<User> list = UserDAO.getAllUsers();
        	request.setAttribute("list", list);
        	request.getRequestDispatcher("AccManager.jsp").forward(request, response);
        }
        else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
		String userRole = (String) request.getSession().getAttribute("role");
    	if (UserDAO.authorize(userRole, "Admin", "Edytor", "User")) {
	        User u = new User();
	        String oldPass = request.getParameter("oldPass");
	        u.setId_User(Integer.parseInt(request.getParameter("id_User")));
	        u.setId_Role(Integer.parseInt(request.getParameter("id_Role")));
	        u.setEmail(request.getParameter("email"));
	        u.setPassw(request.getParameter("passw"));
	        u.setFirst_name(request.getParameter("first_name"));
	        u.setLast_name(request.getParameter("last_name"));
	        u.setSt_index(Integer.parseInt(request.getParameter("st_index")));

	        if(UserDAO.UpdateUser(u,oldPass)==1){
	            if (UserDAO.authorize(userRole, "Admin"))
	                response.sendRedirect("accManager");
	            else
	                response.sendRedirect("userPanel.jsp");
	        }
       	}
       	else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
