package engine.servlets;
import engine.controller.UserDAO;
import engine.model.User;
import engine.model.Test;
import engine.model.Group;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/groupTests")
public class groupTests extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
			int id = Integer.parseInt(request.getParameter("id"));
			ArrayList<Test> testsList = UserDAO.getGroupTests(id);
			ArrayList<Test> otherTestsList = UserDAO.getOtherTests(id);
			Group group = UserDAO.getGroupData(id);
			request.setAttribute("tests", testsList);
			request.setAttribute("otherTests", otherTestsList);
			request.setAttribute("group", group);
			request.getRequestDispatcher("/groupTests.jsp").forward(request, response);
		}
		else {
			response.sendRedirect("/PZ2016/accessDeniedPage.jsp");
		}
    }
}