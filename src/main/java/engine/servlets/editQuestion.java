package engine.servlets;

import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Question;
import engine.model.Test;
import engine.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ArrayList;

import static engine.controller.ConnectionManager.EncodeToUTF;
import static java.lang.Integer.parseInt;

@WebServlet(urlPatterns = "/Tests/editQuestion")
public class editQuestion extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int id = Integer.parseInt(request.getParameter("id"));
            int idTest = Integer.parseInt(request.getParameter("idTest"));
            Question question = TestDAO.getQuestion(id);
            request.setAttribute("id", id);
            request.setAttribute("idTest", idTest);
            request.setAttribute("question", question);
            request.getRequestDispatcher("/EditQuestion.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int id = Integer.parseInt(request.getParameter("id"));
            int idTest = Integer.parseInt(request.getParameter("idTest"));
            String question = request.getParameter("question_name");
            String img = request.getParameter("img");
            if(TestDAO.UpdateQuestion(id, question, img)==1){
                response.sendRedirect("Details?id="+idTest);
            }
            else{
                request.setAttribute("error", EncodeToUTF("Edycja pytania nie powiodła się"));
                request.getRequestDispatcher("Details?id="+idTest).forward(request, response);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
