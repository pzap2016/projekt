package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/removeTest")
public class removeTest extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int id = Integer.parseInt(request.getParameter("id"));
            if(TestDAO.removeTest(id)==1) {
                response.sendRedirect("Tests?succesR=1");
            } else {
                response.sendRedirect("Tests?succesR=0");
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
