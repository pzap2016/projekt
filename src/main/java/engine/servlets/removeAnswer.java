package engine.servlets;

import engine.controller.TestDAO;
import engine.controller.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

/**
 * Created by draxeer on 2017-03-09.
 */
@WebServlet(urlPatterns = "/removeAnswer")
public class removeAnswer extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int id = Integer.parseInt(request.getParameter("idAnswer"));
            int id2 = Integer.parseInt(request.getParameter("idtest"));
            response.setContentType("text/html;charset=UTF-8");
            if (TestDAO.removeAnswer(id) == 1) {
                response.sendRedirect("Tests/Details?id="+id2+"&succesA=1");
            } else {
                response.sendRedirect("Tests/Details?id="+id2+"&succesA=0");
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
