package engine.servlets;

import engine.controller.UserDAO;
import engine.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static engine.controller.ConnectionManager.EncodeToUTF;

/**
 * Created by tomekwwo on 24.03.17.
 */

@WebServlet(urlPatterns = "/changePassword")
public class changeUserPassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {

        int idUser = Integer.parseInt(request.getParameter("id_User"));
        String userPassword = request.getParameter("oldPass");
        String oldPassword = UserDAO.getHash(request.getParameter("passw"));
        String newPassword = request.getParameter("passw_new");
        String newPassword_confirm = request.getParameter("passw_confirm");

        String userRole = (String) request.getSession().getAttribute("role");
    	if (UserDAO.authorize(userRole, "Admin", "Edytor", "User")) {
	        if (userPassword.equals(oldPassword) && newPassword.equals(newPassword_confirm)) {
	            User u = new User();
	            u.setId_User(idUser);
	            u.setId_Role(Integer.parseInt(request.getParameter("id_Role")));
	            u.setEmail(request.getParameter("email"));
	            u.setPassw(newPassword);
	            u.setFirst_name(request.getParameter("first_name"));
	            u.setLast_name(request.getParameter("last_name"));
	            u.setSt_index(Integer.parseInt(request.getParameter("st_index")));

	            if (UserDAO.UpdateUser(u, oldPassword) == 1) {
	                request.setAttribute("error", EncodeToUTF("Hasło zostało zmienione!"));
	                request.getRequestDispatcher("changeUserPassword.jsp?idUser=" + idUser).forward(request, response);
	            }
	            else {
	                request.setAttribute("error", EncodeToUTF("Wystąpił błąd podczas zmiany hasła."));
	                request.getRequestDispatcher("changeUserPassword.jsp?idUser=" + idUser).forward(request, response);
	            }
	        }
	        else {
	            request.setAttribute("error", EncodeToUTF("Podane dane są nieprawidłowe."));
	            request.getRequestDispatcher("changeUserPassword.jsp?idUser=" + idUser).forward(request, response);
	        }
	    }
	    else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
