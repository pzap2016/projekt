package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/Tests/Edit")
public class TestEdit extends HttpServlet{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            Test test = new Test();
            test = TestDAO.getTestDetails(Integer.parseInt(request.getParameter("id")));
            int idTest = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("idTest", idTest);
            request.setAttribute("test", test);
            request.getRequestDispatcher("/testEdit.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
            int idTest = Integer.parseInt(request.getParameter("idTest"));
            String testName = request.getParameter("test_name");
            int testQuestions = Integer.parseInt(request.getParameter("test_amount"));
            int testTime = Integer.parseInt(request.getParameter("test_time"));
            if(TestDAO.UpdateTest(idTest, testName, testQuestions, testTime)==1){
                response.sendRedirect("/PZ2016/Tests");
            }
            else{
                request.setAttribute("error", EncodeToUTF("Wystąpił błąd w trakcie edycji testu"));
                request.getRequestDispatcher("/PZ2016/Tests").forward(request, response);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
