package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Answer;
import engine.model.Question;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.System.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import static engine.controller.ConnectionManager.EncodeToUTF;


/**
 * Created by draxeer on 2016-12-10.
 */
@WebServlet(urlPatterns = "/addQuestion")
public class AddQuestion extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
    	if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
    		// Rzucic na widok dodawania pytań z nazwą testu i jego id
	        int id = Integer.parseInt(request.getParameter("id"));
	        String name = TestDAO.getTestById(id).getTestName();
	        request.setAttribute("id", id);
	        request.setAttribute("name", name);
	        request.getRequestDispatcher("AddQuestion.jsp").forward(request, response);
    	}
    	else {
    		response.sendRedirect("accessDeniedPage.jsp");
    	}
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
    	if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
	        Map map = request.getParameterMap();
	        ArrayList<Integer> a = new ArrayList<Integer>();
	        for (Object key : map.keySet()) { // Poczebuje tego do ogarniecia kluczy tj. ilosci parametrow
	            String keyStr = (String) key;
	            for (int i = 0; i < 100; i++)
	                if (keyStr.equals("a" + i)) {
	                    a.add(i);
	                }
	        }
	        Question q = new Question();
	        q.setImg(request.getParameter("img"));
	        q.setQuestion(request.getParameter("question"));
	        ArrayList<Answer> answers = new ArrayList<Answer>();
	        for (int i = 0; i < a.size(); i++) {
	            Answer tmp = new Answer();
	            if (request.getParameter("trufalse" + a.get(i)).equals("true")){
	                tmp.setIs_Correct(Boolean.TRUE);
	            }
	            tmp.setAnswer(request.getParameter("a" + a.get(i)));
	            if (request.getParameter("trufalse" + a.get(i)).equals("false")){
	                tmp.setIs_Correct(Boolean.FALSE);
	            }
	            answers.add(tmp);
	        }
	        q.setAnswerList(answers);
	        q.setId_Test(Integer.parseInt(request.getParameter("id_test"))); // ODKOMENTOWAC JAK ZROBISZ DOGET`a
	        if(TestDAO.AddQuestionToTest(q)==1){
				response.sendRedirect("Tests/Details?id="+ Integer.parseInt(request.getParameter("id_test")) +"&succesad=1");
	        }
	        else{
	            response.sendRedirect("addQuestion?id="+ Integer.parseInt(request.getParameter("id_test"))+"&succesad=0");
	        }
	    }
	    else {
    		response.sendRedirect("accessDeniedPage.jsp");
    	}
    }
}

