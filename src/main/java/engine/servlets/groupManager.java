package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Group;
import engine.model.Test;
import engine.model.User;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/groupManager")
public class groupManager extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
    	    request.getRequestDispatcher("groupManager.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
            Group g = new Group();
            g.setGroupName(request.getParameter("groupName"));

            if(UserDAO.createGroup(g)==1){
                int i = UserDAO.getGroupByName(g.getGroupName());
                if(i==-1){
                    request.setAttribute("error", EncodeToUTF("Błąd przy dodawaniu grupy."));
                    request.getRequestDispatcher("groupManager.jsp").forward(request, response);
                }
                else{
                    response.sendRedirect("groupManager.jsp");
                }
            }
            else{
                request.setAttribute("error", EncodeToUTF("Błąd przy dodawaniu grupy."));
                request.getRequestDispatcher("groupManager.jsp").forward(request, response);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
