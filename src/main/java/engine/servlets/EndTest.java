package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Answer;
import engine.model.Question;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import javax.servlet.http.HttpSession;

/**
 * Created by draxeer on 2017-03-26.
 */
@WebServlet(urlPatterns = "/End")
public class EndTest extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userRole = (String) session.getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin", "Edytor", "User")) {
	        ArrayList<Question> qpool = (ArrayList<Question>) session.getAttribute("qpool");
			ArrayList<Question>	q = (ArrayList<Question>) session.getAttribute("answers");

	        Integer idTest = (Integer) session.getAttribute("idTEST");
	        Integer idUser = (Integer) session.getAttribute("idUser");

			for(Question qq : qpool){ // UZUPELNIANIE W RAZIE BRAKU ODPOWIEDZI NA JAKIES PYT
				int contains = 0;
				for(Question aq : q){
					if(qq.getId_Question()==aq.getId_Question()){
						contains = 1;
					}
				}
				if(contains==0){
					Question tmp = new Question();
					tmp.setId_Question(qq.getId_Question());
					q.add(tmp);
				}
			}

	        int suma=0,max=0;
	        for(Question qu : qpool){
	            int i=0;
	            for(i=0;i<q.size();i++){
	                if(qu.getId_Question()==q.get(i).getId_Question()) break;
	            }
	            int pkt = 0;
	            int blad = 0;
	            for(Answer an : qu.getAnswerList()){
	                if(an.getIs_Correct()==true) {
	                    max++;
	                }
	                for(Answer a : q.get(i).getAnswerList()){
	                    if(an.getId_Answer() == a.getId_Answer()){
	                        if(an.getIs_Correct()==true){
	                            pkt++;
	                        }
	                        if(an.getIs_Correct()==false){
	                            blad = 1;
	                        }
	                    }
	                }
	            }
	            if(blad==0){
	                suma+=pkt;
	            }
	        }
	        float result = (float)suma/ (float) max;
	        int t = TestDAO.addResult(result,idTest,idUser);
	        if(t>0){
	        	for(Question Q: q){
	        		for(Answer A: Q.getAnswerList()){
	        			TestDAO.addToHistory(t, Q.getId_Question(), A.getId_Answer());
	        		}
	        	}
				session.removeAttribute("qpool");
				session.removeAttribute("time");
				session.removeAttribute("answers");
				session.removeAttribute("index");
				session.removeAttribute("idTEST");
	        	if(request.getParameter("timeover")==null) {
					response.sendRedirect("/PZ2016/Tests/History");
					return;
				}
				else{
					response.sendRedirect("/PZ2016/Tests/History?timeover=1");
					return;
				}
	        }
	    }
		else {
			response.sendRedirect("accessDeniedPage.jsp");
		}
    }
}