package engine.servlets;

import engine.controller.TestDAO;
import engine.model.Answer;
import engine.model.Question;
import engine.model.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

/**
 * Created by draxeer on 2017-03-26.
 */
@WebServlet(urlPatterns = "/Solve")
public class RunTest extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userRole = (String) session.getAttribute("role");
        // sprawdzam, czy użytkownik jest zalogowany
        if (userRole != null) {
            // sprawdzam, czy użytkownik może podejść do żądanego testu
            if (canDoTest(userRole, (Integer) session.getAttribute("idUser"), Integer.parseInt(request.getParameter("idTest")))) {
                if (session.getAttribute("qpool") == null) {
                    ArrayList<Question> Qpool = TestDAO.getQuestionsPool(Integer.parseInt(request.getParameter("idTest")));
                    int mins = TestDAO.getTestById(Integer.parseInt(request.getParameter("idTest"))).getTime_per_test();
                    session.setAttribute("idTEST", Integer.parseInt(request.getParameter("idTest")));
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    Date date2 = addMinutesToDate(mins, date);
                    session.setAttribute("time", dateFormat.format(date2));

                    if (!Qpool.isEmpty()) {
                        session.setAttribute("qpool", Qpool);
                        if (session.getAttribute("index") == null) {
                            session.setAttribute("index", 0);
                            request.getRequestDispatcher("SolveTest.jsp").forward(request, response);
                        }
                    } else {
                        request.setAttribute("error", EncodeToUTF("Test nie zawiera wystarczającej ilości poprawnych pytań, zgłoś błąd do Administratora."));
                        request.getRequestDispatcher("Tests").forward(request, response);

                    }
                } else {
                    int i = (Integer) session.getAttribute("index");
                    ArrayList<Question> Qpool = (ArrayList<Question>) session.getAttribute("qpool");
                    if (i < Qpool.size()) {
                        request.getRequestDispatcher("SolveTest.jsp").forward(request, response);
                    }
                }
            }
            else {
                response.sendRedirect("accessDeniedPage.jsp");
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userRole = (String) session.getAttribute("role");
        if (userRole != null) {
            ArrayList<Question> qpool = (ArrayList<Question>) session.getAttribute("qpool");
            int size=qpool.size();
            if(request.getParameter("index")==null) {
                if (session.getAttribute("answers") == null) {
                    ArrayList<Question> q = new ArrayList<Question>();
                    String[] checkedAnswers = request.getParameterValues("answers");
                    Question tmp = new Question();
                    tmp.setId_Question(Integer.parseInt(request.getParameter("idQuestion")));

                    if(checkedAnswers != null) {
                        for (String s : checkedAnswers) {
                            Answer a = new Answer();
                            a.setId_Answer(Integer.parseInt(s));
                            tmp.getAnswerList().add(a);
                        }
                    }
                    q.add(tmp);
                    session.setAttribute("answers", q);
                }
                else{
                    ArrayList<Question> q = (ArrayList<Question>) session.getAttribute("answers");
                    String[] checkedAnswers = request.getParameterValues("answers");
                    Question tmp = new Question();
                    tmp.setId_Question(Integer.parseInt(request.getParameter("idQuestion")));

                    if(!q.isEmpty()) {
                        for (Question s : q) {
                            if (s.getId_Question() == tmp.getId_Question()) {
                                q.remove(s);
                                break;
                            }
                        }
                    }
                    if(checkedAnswers != null) {
                        for (String s : checkedAnswers) {
                            Answer a = new Answer();
                            a.setId_Answer(Integer.parseInt(s));
                            tmp.getAnswerList().add(a);
                        }
                    }
                    q.add(tmp);
                    session.setAttribute("answers", q);
                }
            }

            int i = (Integer)  session.getAttribute("index");
            i++;
            if(request.getParameter("index")!=null){
                i = Integer.parseInt(request.getParameter("index"));
            }
            if(size==i){
                response.sendRedirect("End");
                return;
            }

            session.setAttribute("index", i);
            request.getRequestDispatcher("SolveTest.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }

    private static Date addMinutesToDate(int minutes, Date beforeTime){
        final long ONE_MINUTE_IN_MILLIS = 60000; //millisecs
        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }

    private boolean canDoTest(String userRole, int idUser, int idTest) {
        if(!userRole.equals("Admin")){
            ArrayList<Test> userTests = TestDAO.getUserTests(idUser);
            for (Test test : userTests) {
                if (test.getId_Test() == idTest)
                    return true;
            }
            return false;
        }
        else
            return true;
    }
}