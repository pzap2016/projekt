package engine.servlets;
import engine.controller.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Groups/removeUserFromGroup")
public class removeUserFromGroup extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
            int idUser = Integer.parseInt(request.getParameter("idUser"));
            int idGroup = Integer.parseInt(request.getParameter("idGroup"));

            if(UserDAO.RemoveUserFromGroup(idUser, idGroup)==1) {
                response.sendRedirect("Users?id="+idGroup);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}