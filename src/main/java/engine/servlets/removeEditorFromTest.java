package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/removeEditorFromTest")
public class removeEditorFromTest extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
            int idUser = Integer.parseInt(request.getParameter("idUser"));
            int idTest = Integer.parseInt(request.getParameter("idTest"));

            if(TestDAO.RemoveEditorFromTest(idUser, idTest)==1) {
                response.sendRedirect("Editors?id="+idTest);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}