package engine.servlets;
import engine.controller.UserDAO;
import engine.model.User;
import java.io.IOException;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

/**
 * Created by draxeer on 2016-12-10.
 */
@WebServlet(urlPatterns = "/Register")
public class Register extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (userRole == null) {
            User u = new User();

            u.setEmail(request.getParameter("email"));
            u.setFirst_name(request.getParameter("first_name"));
            u.setLast_name(request.getParameter("last_name"));
            u.setPassw(request.getParameter("passw"));
            String confirmation = request.getParameter("passw_confirmation");
            u.setSt_index(Integer.parseInt(request.getParameter("st_index")));
            u.setId_Role(3); // Ustawiamy jako zwykłego użytk. domyślnie przy rejestracji.

            if (u.getPassw().equals(confirmation)) {
                if (UserDAO.RegisterUser(u) == 1) {
                    response.sendRedirect("index.jsp");
                } else {
                    request.setAttribute("error", EncodeToUTF("Błąd rejestracji. Podany adres e-mail, lub indeks studenta są już w użyciu."));
                    request.getRequestDispatcher("register.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("error", EncodeToUTF("Błąd rejestracji. Podane hasła nie są takie same."));
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
