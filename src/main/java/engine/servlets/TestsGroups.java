package engine.servlets;
import engine.controller.UserDAO;
import engine.model.Group;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests/Groups")
public class TestsGroups extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin", "Edytor")) {
	        ArrayList<Group> list = UserDAO.getAllGroups();
	        request.setAttribute("groups", list);
	        request.getRequestDispatcher("/TestsGroups.jsp").forward(request, response);
	    }
		else {
			response.sendRedirect("/PZ2016/accessDeniedPage.jsp");
		}
    }
}
