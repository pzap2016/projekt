package engine.servlets;
import engine.controller.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

/**
 * Created by draxeer on 2016-12-10.
 */
@WebServlet(urlPatterns = "/Login")
public class Login extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (userRole == null) {
            //POBRANIE ATRYBUTOW  + UTWORZENIE SESJI + WERYFIKACJA POPRZEZ UserDAO
            String user = request.getParameter("user");
            String passw = request.getParameter("passw");

            if(UserDAO.Login(user,passw)==1){ // JESLI DANE POPRAWNE

                HttpSession session = request.getSession();
                session.setAttribute("username", user);
                session.setAttribute("role", UserDAO.getUserRole(user));
                session.setAttribute("idUser", UserDAO.getUserId(user));
                request.setAttribute("zal", "1asd");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
            else{
                request.setAttribute("error", EncodeToUTF("Podano błędne dane"));
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }
        else {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
	}
}
