package engine.servlets;
import engine.controller.UserDAO;
import engine.model.User;
import engine.model.Test;
import engine.model.Group;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Groups/Users")
public class groupUsers extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userRole = (String) request.getSession().getAttribute("role");
		if (UserDAO.authorize(userRole, "Admin")) {
			int id = Integer.parseInt(request.getParameter("id"));
			ArrayList<User> membersList = UserDAO.getGroupUsers(id);
			ArrayList<User> otherList = UserDAO.getOtherUsers(id);
			Group group = UserDAO.getGroupData(id);
			request.setAttribute("users", membersList);
			request.setAttribute("otherUsers", otherList);
			request.setAttribute("group", group);
			request.getRequestDispatcher("/groupUsers.jsp").forward(request, response);
		}
		else {
			response.sendRedirect("accessDeniedPage.jsp");
		}
    }
}