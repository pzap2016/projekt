package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.SolvedTests;
import engine.model.Test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Groups/History")
public class groupHistory extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	String role = (String) session.getAttribute("role");
    	
    	if (UserDAO.authorize(role, "Admin", "Edytor", "User")) {
    		int id = Integer.parseInt(request.getParameter("id"));
        	ArrayList<SolvedTests> history = TestDAO.getGroupSolvedTests(id);
        	request.setAttribute("history", history);
            request.getRequestDispatcher("/groupHistory.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}