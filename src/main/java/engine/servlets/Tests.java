package engine.servlets;
import engine.controller.TestDAO;
import engine.controller.UserDAO;
import engine.model.Test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/Tests")
public class Tests extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	ArrayList<Test> list = new ArrayList<Test>();
    	HttpSession session = request.getSession();
    	String role = (String) session.getAttribute("role");
    	
    	if (UserDAO.authorize(role, "Admin", "Edytor", "User")) {
	    	int idUser = (Integer) session.getAttribute("idUser");
	    	if (role.equals("Admin")){
	    		list = TestDAO.getAllTests();
	    	} else if (role.equals("User")) {
	    		list = TestDAO.getUserTests(idUser);
	    	} else if (role.equals("Edytor")) {
	    		list = TestDAO.getEditorTests(idUser);
	    	}
	    	if(request.getAttribute("error")!=null){
				request.setAttribute("error", request.getAttribute("error"));
			}
    	
    		request.setAttribute("tests", list);
        	request.getRequestDispatcher("ViewTests.jsp").forward(request, response);	
    	}
    	else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }

}
