package engine.servlets;
import engine.controller.UserDAO;
import engine.model.Group;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static engine.controller.ConnectionManager.EncodeToUTF;

@WebServlet(urlPatterns = "/CreateGroup")
public class CreateGroup extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
        	request.getRequestDispatcher("createGroup.jsp").forward(request, response);
    	}
    	else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
        Group g = new Group();
        g.setGroupName(request.getParameter("groupName"));
        
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
	        if(UserDAO.createGroup(g)==1){
	            int i = UserDAO.getGroupByName(g.getGroupName());
	            if(i==-1){
	                request.setAttribute("error", EncodeToUTF("Wystąpił błąd w trakcie dodawania grupy."));
	                request.getRequestDispatcher("groupManager.jsp").forward(request, response);
	            }
	            else{
	                response.sendRedirect("groupManager.jsp");
	            }
	        }
	        else{
	            request.setAttribute("error", EncodeToUTF("Wystąpił błąd w trakcie dodawania grupy."));
	            request.getRequestDispatcher("groupManager.jsp").forward(request, response);
	        }
	    }
        else {
        	response.sendRedirect("accessDeniedPage.jsp");
        }
    }
}
