package engine.servlets;
import engine.controller.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Created by draxeer on 2016-12-18.
 */
@WebServlet(urlPatterns = "/removeUser")
public class removeUser extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userRole = (String) request.getSession().getAttribute("role");
        if (UserDAO.authorize(userRole, "Admin")) {
            int id = Integer.parseInt(request.getParameter("idUser"));

            if(UserDAO.removeUser(id)==1) { // JESLI DANE POPRAWNE
                response.sendRedirect("accManager");
            }
        }
        else {
            response.sendRedirect("accessDeniedPage.jsp");
        }
    }


}