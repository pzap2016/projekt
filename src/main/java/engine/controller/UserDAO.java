package engine.controller;
import engine.model.Test;
import engine.model.User;
import engine.model.Group;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by draxeer on 2016-12-10.
 */
public class UserDAO {
    static Connection conn;
    static Statement statement;

    public static void getConnection(){
        conn = ConnectionManager.getConnection();
        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized boolean authorize(String userRole, String... rolesAccepted) {
        if (rolesAccepted == null)
            return false;
        else {
            for (String role : rolesAccepted) {
                if (role.equals(userRole))
                    return true;
            }
        }
    	return false;
    }

    public static synchronized String getHash(String plaintext){
        String hashtext = "";
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plaintext.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            hashtext = bigInt.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
        }
        catch(Exception e){
        }
        return hashtext;
    }
    public static synchronized int RegisterUser(User user){
        try {
            getConnection();

            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE email ='" + user.getEmail() + "'");
            if (rs.next()) {
                return 0;
                }
            rs = statement.executeQuery("SELECT * FROM Users WHERE st_index ='" + user.getSt_index() + "'");
            if (rs.next()) {
                return 0;
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        try {
            String hashpass = getHash(user.getPassw());

            PreparedStatement register = conn.prepareStatement("INSERT INTO Users (first_name, last_name, passw, st_index, email, id_Role) VALUES (?, ?, ?, ?, ?, 3)");
            register.setString(1, user.getFirst_name());
            register.setString(2, user.getLast_name());
            register.setString(3, hashpass);
            register.setInt(4, user.getSt_index());
            register.setString(5, user.getEmail());
            register.executeUpdate();
            conn.close();
            return 1;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return 0;

    }
    public static synchronized int Login(String user, String passw){
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE email ='" + user +"'");
            if(rs.next()) {
                if (getHash(passw).equals(rs.getString("passw"))) {
                    conn.close();
                    return 1;
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized String getUserRole(String user){
        int id = 3;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE email ='" + user +"'");

            if(rs.next()){
                    id = rs.getInt("id_Role");
            }
            rs = statement.executeQuery("SELECT * FROM Roles WHERE id_Role ='" + id +"'");
            if(rs.next()) {
                return rs.getString("Name");
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return "Nie znam tego pana lul";
    }
    
    public static synchronized int getUserId(String user){
    	int id = 0;
	    try{
	    	getConnection();
            ResultSet rs = statement.executeQuery("SELECT id_User FROM Users WHERE email ='" + user +"'");
            if(rs.next()){
            	id = rs.getInt("id_User");
            }
            conn.close();
    	}catch(SQLException e) {
	        e.printStackTrace();
	    }
	    return id;
    }
    
    public static synchronized ArrayList<User> getAllUsers(){
        ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users ");

            while(rs.next()) {
                User tmp = new User();
                tmp.setId_Role(rs.getInt("id_Role"));
                tmp.setId_User(rs.getInt("id_User"));
                tmp.setSt_index(rs.getInt("st_index"));
                tmp.setLast_name(rs.getString("last_name"));
                tmp.setFirst_name(rs.getString("first_name"));
                tmp.setEmail(rs.getString("email"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized ArrayList<User> getAllUsers3(){
        ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE id_Role = 3");

            while(rs.next()) {
                User tmp = new User();
                tmp.setId_Role(rs.getInt("id_Role"));
                tmp.setId_User(rs.getInt("id_User"));
                tmp.setSt_index(rs.getInt("st_index"));
                tmp.setLast_name(rs.getString("last_name"));
                tmp.setFirst_name(rs.getString("first_name"));
                tmp.setEmail(rs.getString("email"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized ArrayList<User> getAllEditors(){
        ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE id_Role = 2");

            while(rs.next()) {
                User tmp = new User();
                tmp.setId_Role(rs.getInt("id_Role"));
                tmp.setId_User(rs.getInt("id_User"));
                tmp.setSt_index(rs.getInt("st_index"));
                tmp.setLast_name(rs.getString("last_name"));
                tmp.setFirst_name(rs.getString("first_name"));
                tmp.setEmail(rs.getString("email"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized User getUserData(int id_User){
        User user = new User();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE id_User = "+ id_User);

            if(rs.next()) {
                user.setId_Role(rs.getInt("id_Role"));
                user.setId_User(rs.getInt("id_User"));
                user.setSt_index(rs.getInt("st_index"));
                user.setLast_name(rs.getString("last_name"));
                user.setFirst_name(rs.getString("first_name"));
                user.setEmail(rs.getString("email"));
                user.setPassw(rs.getString("passw"));
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
    public static synchronized User getUserByName(String name){
        User user = new User();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Users WHERE email =" + "'" + name + "'");
            if(rs.next()){
                user.setId_Role(rs.getInt("id_Role"));
                user.setId_User(rs.getInt("id_User"));
                user.setSt_index(rs.getInt("st_index"));
                user.setLast_name(rs.getString("last_name"));
                user.setFirst_name(rs.getString("first_name"));
                user.setEmail(rs.getString("email"));
                user.setPassw(rs.getString("passw"));
            }
            conn.close();
            return user;

        }catch (SQLException e){
        }
        return user;
    }

    public static synchronized ArrayList<User> getGroupUsers(int id_Group){
    	ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Users.* FROM Users, Group_Users WHERE Users.id_User = Group_Users.id_User AND Group_Users.id_Group =" + id_Group + " AND Users.id_Role = 3");

            while(rs.next()) {
            	User tmp = new User();
                tmp.setId_Role(rs.getInt("id_Role"));
                tmp.setId_User(rs.getInt("id_User"));
                tmp.setSt_index(rs.getInt("st_index"));
                tmp.setLast_name(rs.getString("last_name"));
                tmp.setFirst_name(rs.getString("first_name"));
                tmp.setEmail(rs.getString("email"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized ArrayList<User> getOtherUsers(int id_Group){
    	ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Users.* FROM Users, Group_Users WHERE Users.id_User = Group_Users.id_User AND Group_Users.id_Group =" + id_Group + " AND Users.id_Role = 3");
            if(!rs.isBeforeFirst() ){
            	lista = getAllUsers3();
	        } else {
	        	ResultSet rs2 = statement.executeQuery("SELECT * FROM Users WHERE id_User NOT IN (SELECT Users.id_User FROM Users, Group_Users WHERE Users.id_User = Group_Users.id_User AND Group_Users.id_Group = " + id_Group + " AND Users.id_Role = 3) AND id_Role = 3");
	        	while(rs2.next()) {
	            	User tmp = new User();
	                tmp.setId_Role(rs2.getInt("id_Role"));
	                tmp.setId_User(rs2.getInt("id_User"));
	                tmp.setSt_index(rs2.getInt("st_index"));
	                tmp.setLast_name(rs2.getString("last_name"));
	                tmp.setFirst_name(rs2.getString("first_name"));
	                tmp.setEmail(rs2.getString("email"));
	                lista.add(tmp);
	            }
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized int AddUserToGroup(int idUser, int idGroup){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("INSERT INTO Group_Users (id_Group, id_User) VALUES (?, ?)");
            update.setInt(1, idGroup);
            update.setInt(2, idUser);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int RemoveUserFromGroup(int idUser, int idGroup){
        try{
            getConnection();
            PreparedStatement del = conn.prepareStatement("DELETE FROM Group_Users WHERE id_Group = ? AND id_User = ?");
            del.setInt(1, idGroup);
            del.setInt(2, idUser);
            del.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized ArrayList<Test> getGroupTests(int id_Group){
    	ArrayList<Test> lista = new ArrayList<Test>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Test.* FROM Test, Group_Tests WHERE Test.id_Test = Group_Tests.id_Test AND Group_Tests.id_Group =" + id_Group);

            while(rs.next()) {
            	Test tmp = new Test();
                tmp.setId_Test(rs.getInt("id_Test"));
                tmp.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                tmp.setTestName(rs.getString("Test_Name"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized ArrayList<Test> getOtherTests(int id_Group){
    	ArrayList<Test> lista = new ArrayList<Test>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Users.* FROM Users, Group_Users WHERE Users.id_User = Group_Users.id_User AND Group_Users.id_Group =" + id_Group);
            if(!rs.isBeforeFirst() ){
            	lista = TestDAO.getAllTests();
	        } else {
	        	ResultSet rs2 = statement.executeQuery("SELECT * FROM Test T WHERE NOT EXISTS (SELECT id_Test FROM Group_Tests GT WHERE GT.id_Group = " + id_Group + " AND T.id_Test = GT.id_Test)");
	        	while(rs2.next()) {
	            	Test tmp = new Test();
	                tmp.setId_Test(rs2.getInt("id_Test"));
	                tmp.setAuthor(UserDAO.getUserData(rs2.getInt("id_User")));
	                tmp.setTestName(rs2.getString("Test_Name"));
	                lista.add(tmp);
	            }
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized int AddTestToGroup(int idTest, int idGroup){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("INSERT INTO Group_Tests (id_Group, id_Test) VALUES (?, ?)");
            update.setInt(1, idGroup);
            update.setInt(2, idTest);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int RemoveTestFromGroup(int idTest, int idGroup){
        try{
            getConnection();
            PreparedStatement del = conn.prepareStatement("DELETE FROM Group_Tests WHERE id_Group = ? AND id_Test = ?");
            del.setInt(1, idGroup);
            del.setInt(2, idTest);
            del.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized ArrayList<User> getTestEditors(int id_Test){
    	ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Users.* FROM Users, Test_Editors WHERE Users.id_User = Test_Editors.id_User AND Test_Editors.id_Test =" + id_Test + " AND id_Role = 2");

            while(rs.next()) {
            	User tmp = new User();
                tmp.setId_Role(rs.getInt("id_Role"));
                tmp.setId_User(rs.getInt("id_User"));
                tmp.setSt_index(rs.getInt("st_index"));
                tmp.setLast_name(rs.getString("last_name"));
                tmp.setFirst_name(rs.getString("first_name"));
                tmp.setEmail(rs.getString("email"));
                lista.add(tmp);
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized ArrayList<User> getOtherEditors(int id_Test){
    	ArrayList<User> lista = new ArrayList<User>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Users.* FROM Users, Test_Editors WHERE Users.id_User = Test_Editors.id_User AND Test_Editors.id_Test =" + id_Test + " AND Users.id_Role = 2");
            if(!rs.isBeforeFirst() ){
            	lista = getAllEditors();
	        } else {
	        	ResultSet rs2 = statement.executeQuery("SELECT * FROM Users U WHERE NOT EXISTS (SELECT id_User FROM Test_Editors TE WHERE TE.id_Test = " + id_Test + " AND U.id_User = TE.id_User) AND id_Role = 2");
	        	while(rs2.next()) {
	            	User tmp = new User();
	                tmp.setId_Role(rs2.getInt("id_Role"));
	                tmp.setId_User(rs2.getInt("id_User"));
	                tmp.setSt_index(rs2.getInt("st_index"));
	                tmp.setLast_name(rs2.getString("last_name"));
	                tmp.setFirst_name(rs2.getString("first_name"));
	                tmp.setEmail(rs2.getString("email"));
	                lista.add(tmp);
	            }
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public static synchronized int UpdateUser(User user,String oldPass){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Users SET first_name = ?, last_name = ?, passw = ?, st_index = ?, email = ?, id_Role = ? WHERE id_User = ?");
            update.setString(1, user.getFirst_name());
            update.setString(2, user.getLast_name());
            if(oldPass.equals(user.getPassw())) {
                update.setString(3, user.getPassw());
            }
            else{
                update.setString(3, getHash(user.getPassw()));
            }
            update.setInt(4, user.getSt_index());
            update.setString(5, user.getEmail());
            update.setInt(6, user.getId_Role());
            update.setInt(7, user.getId_User());
            update.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int removeUser(int user){
        try{
            getConnection();
            PreparedStatement del = conn.prepareStatement("DELETE FROM Users WHERE id_User = ?");
            PreparedStatement del2 = conn.prepareStatement("UPDATE Test SET id_User = NULL WHERE id_User = ?");
            PreparedStatement del3 = conn.prepareStatement("DELETE FROM Group_Users WHERE id_User = ?");
            PreparedStatement del4 = conn.prepareStatement("DELETE FROM Test_Editors WHERE id_User = ?");
            del.setInt(1, user);
            del2.setInt(1, user);
            del3.setInt(1, user);
            del4.setInt(1, user);
            del.executeUpdate();
            del2.executeUpdate();
            del3.executeUpdate();
            del4.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int createGroup(Group group){
        try{
            getConnection();
            PreparedStatement create = conn.prepareStatement("INSERT INTO Groups (Group_Name) VALUES (?);");
            create.setString(1, group.getGroupName());
            create.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized ArrayList<Group> getAllGroups(){
        ArrayList<Group> grupy = new ArrayList<Group>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Groups");

            while(rs.next()) {
                Group grupa = new Group();
                grupa.setId_Group(rs.getInt("id_Group"));
                grupa.setGroupName(rs.getString("Group_Name"));
                grupy.add(grupa);
            }
            conn.close();
            return grupy;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return grupy;
    }
    
    public static synchronized Group getGroupData(int id_Group){
        Group group = new Group();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Groups WHERE id_Group = "+ id_Group);

            if(rs.next()) {
            	group.setId_Group(rs.getInt("id_Group"));
            	group.setGroupName(rs.getString("Group_Name"));
            }
            conn.close();
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return group;
    }
    
    public static synchronized int getGroupByName(String name){
        int i = -1;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Groups WHERE Group_Name =" + "'" + name + "'");
            if(rs.next()){
                i = rs.getInt("id_Group");
            }
            conn.close();
        }catch(SQLException e){

        }
        return i;
    }
    
    public static synchronized int UpdateGroup(Group group){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Groups SET Group_Name = ? WHERE id_Group = ?");
            update.setInt(1, group.getId_Group());
            update.setString(2, group.getGroupName());
            update.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int removeGroup(int group){
        try{
            getConnection();
            PreparedStatement del = conn.prepareStatement("DELETE FROM Groups WHERE id_Group = ?");
            PreparedStatement del2 = conn.prepareStatement("DELETE FROM Group_Users WHERE id_Group = ?");
            PreparedStatement del3 = conn.prepareStatement("DELETE FROM Group_Tests WHERE id_Group = ?");
            del.setInt(1, group);
            del2.setInt(1, group);
            del3.setInt(1, group);
            del.executeUpdate();
            del2.executeUpdate();
            del3.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
