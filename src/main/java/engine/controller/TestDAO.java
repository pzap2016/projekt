package engine.controller;
import engine.model.Answer;
import engine.model.Question;
import engine.model.SolvedTests;
import engine.model.SolvedTestsHistory;
import engine.model.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by draxeer on 2017-01-21.
 */
public class TestDAO {
    static Connection conn;
    static Statement statement;

    public static void getConnection(){
        conn = ConnectionManager.getConnection();
        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static synchronized ArrayList<Question> getQuestionsPool(int id){
        int z = -1;
        ArrayList<Integer> all = new ArrayList<Integer>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT id_Question FROM Questions WHERE id_Test =" + id + " AND id_Question IN (SELECT a.id_Question FROM Question_Answers a WHERE EXISTS (SELECT NULL FROM Question_Answers c WHERE c.id_Question = a.id_Question GROUP BY c.id_Question HAVING COUNT(*) >= 2)) AND id_Question IN (SELECT id_Question FROM Question_Answers WHERE id_Question_Answer IN (SELECT id_Question_Answer FROM Question_Answers WHERE Is_Correct = 1))");
            while(rs.next()) {
                all.add(rs.getInt("id_Question"));
            }
            rs = statement.executeQuery("SELECT Questions_per_test FROM Test WHERE id_Test =" + id);
            if(rs.next()){
                z = rs.getInt("Questions_per_test");
            }
            conn.close();
        }catch(SQLException e){
            return new ArrayList<Question>();
        }
        ArrayList<Question> list = new ArrayList<Question>();
        if(all.size()>=z) { // Pytań możliwych do zadania musi być przynajmniej Questions_per_test !
            if (!all.isEmpty()) {
                Collections.shuffle(all);
                for (int i = 0; i < z; i++) {
                    list.add(getQuestion(all.get(i)));
                }
            }
        }
        return list;
    }

    public static synchronized int UpdateQuestion(int id, String q, String img){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Questions SET Question = ?, img = ? WHERE id_Question = ?");
            update.setString(1, q);
            update.setString(2, img);
            update.setInt(3, id);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){}
        return 0;
    }

    public static synchronized int UpdateAnswer(int id, int answer){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Question_Answers SET Is_Correct = ? WHERE id_Question_Answer = ?");
            update.setInt(1, answer);
            update.setInt(2, id);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){}
        return 0;
    }
    
    public static synchronized int UpdateAnswerText(int id, String answer){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Question_Answers SET Answer = ? WHERE id_Question_Answer = ?");
            update.setString(1, answer);
            update.setInt(2, id);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){}
        return 0;
    }
    
    public static synchronized int UpdateTest(int id, String testName, int questionsPerTest, int timePerTest){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("UPDATE Test SET Test_Name = ?, Questions_per_test = ?, Time_per_test = ? WHERE id_Test = ?");
            update.setString(1, testName);
            update.setInt(2, questionsPerTest);
            update.setInt(3, timePerTest);
            update.setInt(4, id);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){}
        return 0;
    }
    
    public static synchronized int AddQuestionToTest(Question q){
        try{
            getConnection();
            // uzupełnić metodę
            PreparedStatement insert = conn.prepareStatement("INSERT INTO Questions (id_Question, id_Test, Question, img) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            insert.setInt(1, q.getId_Question());
            insert.setInt(2, q.getId_Test());
            insert.setString(3, q.getQuestion());
            insert.setString(4, q.getImg());
            int dodane = insert.executeUpdate();

            if(dodane == 0){
                return 0;
            }
            int id_Question = -1;
            ResultSet generatedKeys = insert.getGeneratedKeys();
            if (generatedKeys.next()) {
                    id_Question = generatedKeys.getInt(1);
            }
            else{
                System.out.println("EHE");
                return 0;
             }
            conn.close();
            if(addAnswers(id_Question, q.getAnswerList())==1) {
                return 1;
            }
        }catch (SQLException e){
        }
        return 0;
    }

    public static synchronized Question getQuestion(int id){
        Question q = new Question();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Questions WHERE id_Question =" + id);
            if(rs.next()){
                q.setId_Test(rs.getInt("id_Test"));
                q.setQuestion(rs.getString("Question"));
                q.setId_Question(id);
                q.setImg(rs.getString("img"));
                conn.close();
                q.setAnswerList(getAnswers(id));
                return q;
            }
            conn.close();
        }catch(SQLException e){
        }
        return q;
    }
    
    public static synchronized String getAnswerText(int id){
    	String a = null;
    	try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Answer FROM Question_Answers WHERE id_Question_Answer =" + id);
            if(rs.next()){
                a = rs.getString("Answer");
                conn.close();
                return a;
            }
            conn.close();
        }catch(SQLException e){
        }
        return a;
    }
    
    public static synchronized int getAnswer(int id){
        int a = 0;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Is_Correct FROM Question_Answers WHERE id_Question_Answer =" + id);
            if(rs.next()){
                a = rs.getInt("Is_Correct");
                conn.close();
                return a;
            }
            conn.close();
        }catch(SQLException e){
        }
        return a;
    }
    
    public static synchronized int addAnswers(int id, List<Answer> a) {
        try{
            getConnection();

            for(int i=0;i<a.size();i++) {
                PreparedStatement insert = conn.prepareStatement("INSERT INTO Question_Answers(id_Question, Answer, Is_Correct) VALUES (?, ?, ?)");
                insert.setInt(1, id);
                insert.setString(2, a.get(i).getAnswer());
                insert.setBoolean(3, a.get(i).getIs_Correct());
                insert.executeUpdate();
            }

            conn.close();
            return 1;
        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized int removeTest(int id){
        try{
        	removeTestDetails(id);
            getConnection();
            PreparedStatement delete = conn.prepareStatement("DELETE FROM Test WHERE id_Test = ?");
            delete.setInt(1, id);
            delete.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int removeTestDetails(int id){
        try{
        	int tmp = 0;
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Questions WHERE id_Test =" + id);
            while (rs.next()){
            	tmp = 1;
                removeQuestion(rs.getInt("id_Question"));
            }
            if(tmp == 1){
            	getConnection();
            }
            PreparedStatement delete = conn.prepareStatement("DELETE FROM Group_Tests WHERE id_Test = ?");
            PreparedStatement delete2 = conn.prepareStatement("DELETE FROM Test_Editors WHERE id_Test = ?");
            delete.setInt(1, id);
            delete2.setInt(1, id);
            delete.executeUpdate();
            delete2.executeUpdate();
            conn.close();
            return 1;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int removeQuestion(int id){
        try{
            getConnection();
            PreparedStatement delete = conn.prepareStatement("DELETE FROM Questions WHERE id_Question = ?");
            PreparedStatement delete2 = conn.prepareStatement("DELETE FROM Question_Answers WHERE id_Question = ?");
            ResultSet rs = statement.executeQuery("SELECT * FROM Question_Answers WHERE id_Question =" + id);
            delete.setInt(1, id);
            delete2.setInt(1, id);
            if(rs.next()) {
                delete2.executeUpdate();
            }
            delete.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized int removeAnswer(int id){
        try{
            getConnection();
            PreparedStatement delete = conn.prepareStatement("DELETE FROM Question_Answers WHERE id_Question_Answer = ?");
            delete.setInt(1, id);
            delete.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized Answer addAnswer(Answer a){
        Answer answer = new Answer();
        try{
            getConnection();
            PreparedStatement insert = conn.prepareStatement("INSERT INTO Question_Answers (id_Question, Answer, Is_Correct) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            insert.setInt(1, a.getId_Question());
            insert.setString(2, a.getAnswer());
            insert.setBoolean(3, a.getIs_Correct());
            insert.executeUpdate();
            int id_Answer = -1;
            ResultSet generatedKeys = insert.getGeneratedKeys();
            if (generatedKeys.next()) {
                id_Answer = generatedKeys.getInt(1);
            }
            else{
                conn.close();
                return answer;
            }
            answer.setIs_Correct(a.getIs_Correct());
            answer.setAnswer(a.getAnswer());
            answer.setId_Question(a.getId_Question());
            answer.setId_Answer(id_Answer);
            conn.close();
            return answer;

        }catch(SQLException e){
        }
        return answer;
    }

    public static synchronized int AddEditorToTest(int idUser, int idTest){
        try{
            getConnection();
            PreparedStatement update = conn.prepareStatement("INSERT INTO Test_Editors (id_Test, id_User) VALUES (?, ?)");
            update.setInt(1, idTest);
            update.setInt(2, idUser);
            update.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized int RemoveEditorFromTest(int idUser, int idTest){
        try{
            getConnection();
            PreparedStatement del = conn.prepareStatement("DELETE FROM Test_Editors WHERE id_Test = ? AND id_User = ?");
            del.setInt(1, idTest);
            del.setInt(2, idUser);
            del.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public static synchronized ArrayList<Test> getAllTests(){
        ArrayList<Test> Tests = new ArrayList<Test>();
        try {
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test");

            while(rs.next()) {
                Test tmp = new Test();
                tmp.setId_Test(rs.getInt("id_Test"));
                tmp.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                tmp.setTestName(rs.getString("Test_Name"));
                tmp.setQuestions_per_test(rs.getInt("Questions_per_test"));
                tmp.setTime_per_test(rs.getInt("Time_per_test"));
                Tests.add(tmp);
            }
            conn.close();
            return Tests;
        }
        catch(SQLException e){
        }
        return Tests;
    }
    
    public static synchronized ArrayList<Test> getUserTests(int idUser){
        ArrayList<Test> Tests = new ArrayList<Test>();
        try {
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test WHERE id_test IN (SELECT id_test FROM Group_Tests WHERE id_Group IN (SELECT id_Group FROM Group_Users WHERE id_user = " + idUser + "))");

            while(rs.next()) {
                Test tmp = new Test();
                tmp.setId_Test(rs.getInt("id_Test"));
                tmp.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                tmp.setTestName(rs.getString("Test_Name"));
                tmp.setQuestions_per_test(rs.getInt("Questions_per_test"));
                tmp.setTime_per_test(rs.getInt("Time_per_test"));
                Tests.add(tmp);

            }
            conn.close();
            return Tests;
        }
        catch(SQLException e){
        }
        return Tests;
    }
    
    public static synchronized ArrayList<Test> getEditorTests(int idUser){
        ArrayList<Test> Tests = new ArrayList<Test>();
        try {
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test WHERE id_test IN (SELECT id_Test FROM Test_Editors WHERE id_User = " + idUser + ")");

            while(rs.next()) {
                Test tmp = new Test();
                tmp.setId_Test(rs.getInt("id_Test"));
                tmp.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                tmp.setTestName(rs.getString("Test_Name"));
                Tests.add(tmp);
            }
            conn.close();
            return Tests;
        }
        catch(SQLException e){
        }
        return Tests;
    }
    
    public static synchronized int createTest(Test test){
        try{
            getConnection();
            PreparedStatement create = conn.prepareStatement("INSERT INTO Test (id_User, Test_Name, Questions_per_test, Time_per_test) VALUES (?, ?, ?, ?)");
            create.setInt(1, test.getAuthor().getId_User());
            create.setString(2, test.getTestName());
            create.setInt(3,test.getQuestions_per_test());
            create.setInt(4,test.getTime_per_test());
            create.executeUpdate();
            conn.close();
            return 1;

        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized int getTestByName(String name){
        int i = -1;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test WHERE Test_Name =" + "'" + name + "'");
            if(rs.next()){
                i = rs.getInt("id_Test");
            }
            conn.close();
        }catch(SQLException e){

        }
        return i;
    }
    
    public static synchronized String getTestName(int id){
        String i = null;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Test_Name FROM Test WHERE id_Test = " + id);
            if(rs.next()){
                i = rs.getString("Test_Name");
            }
            conn.close();
        }catch(SQLException e){

        }
        return i;
    }
    
    public static synchronized Test getTestById(int idTest){
        Test test = new Test();
        try {
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test WHERE id_Test = "+ idTest);
            while(rs.next()) {
                test.setId_Test(rs.getInt("id_Test"));
                test.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                test.setTestName(rs.getString("Test_Name"));
                test.setQuestions_per_test(rs.getInt("Questions_per_test"));
                test.setTime_per_test(rs.getInt("Time_per_test"));
            }
            conn.close();
            return test;
        }
        catch(SQLException e){
        }
        return test;
    }
    
    public static synchronized Test getTestDetails(int id){
        Test t = new Test();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Test WHERE id_Test =" + id);
            if(rs.next()){
            	t.setId_Test(id);
                t.setTestName(rs.getString("Test_Name"));
                t.setAuthor(UserDAO.getUserData(rs.getInt("id_User")));
                t.setQuestions_per_test(rs.getInt("Questions_per_test"));
                t.setTime_per_test(rs.getInt("Time_per_test"));
                t.setId_Test(id);
            }
            rs = statement.executeQuery("SELECT * FROM Questions WHERE id_Test =" + id);
            List<Question> s = t.getQuestionList();
            while(rs.next()){
                Question q = new Question();
                q.setId_Question(rs.getInt("id_Question"));
                q.setQuestion(rs.getString("Question"));
                q.setImg(rs.getString("img"));
                q.setAnswerList(getAnswers(rs.getInt("id_Question")));
                s.add(q);
            }
            t.setQuestionList(s);
            conn.close();
            return t;
        }catch(SQLException e){
        }
        return t;
    }
    
    public static synchronized List<Answer> getAnswers(int id){
        List<Answer> l = new ArrayList<Answer>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM Question_Answers WHERE id_Question =" + id);
            while(rs.next()){
                Answer a = new Answer();
                a.setId_Answer(rs.getInt("id_Question_Answer"));
                a.setAnswer(rs.getString("Answer"));
                a.setIs_Correct(rs.getBoolean("Is_Correct"));
                l.add(a);
            }
            conn.close();
            return l;
        }catch(SQLException e){}

        return l;
    }
    
    public static synchronized int addResult(float result, int id_test, int id_user){
        int id = -1;

        try{
            getConnection();
            PreparedStatement insert = conn.prepareStatement("INSERT INTO SolvedTests (id_User, id_Test, score, date) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            insert.setInt(1, id_user);
            insert.setInt(2, id_test);
            insert.setFloat(3, result);
            insert.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
            insert.executeUpdate();

            ResultSet generatedKeys = insert.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getInt(1);
            }
            else{
                conn.close();
                return id;
            }
            conn.close();
            return id;

        }catch(SQLException e){
        }
        return id;
    }
    
    public static synchronized int addToHistory(int id_SolvedTest, int id_Question, int id_Answer){
        try{
            getConnection();
            PreparedStatement insert = conn.prepareStatement("INSERT INTO SolvedTests_History (id_SolvedTest, id_Question, id_Answer) VALUES (?, ?, ?)");
            insert.setInt(1, id_SolvedTest);
            insert.setInt(2, id_Question);
            insert.setInt(3, id_Answer);
            insert.executeUpdate();
            conn.close();
            return 1;
        }catch(SQLException e){
        }
        return 0;
    }
    
    public static synchronized ArrayList<SolvedTests> getUserSolvedTests(int idUser){
    	ArrayList<SolvedTests> st = new ArrayList<SolvedTests>();
        float tmpScore = 0;
        int score = 0;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM SolvedTests WHERE id_User = " + idUser + " ORDER BY date DESC");
            while(rs.next()){
            	SolvedTests tmp = new SolvedTests();
            	tmp.setId_SolvedTest(rs.getInt("id_SolvedTest"));
            	tmp.setTestName(getTestName(rs.getInt("id_Test")));
            	tmp.setId_User(rs.getInt("id_User"));
            	tmpScore = rs.getFloat("score");
            	score = (int) (tmpScore * 100);
            	tmp.setScore(score);
            	tmp.setDate(rs.getString("date"));
            	st.add(tmp);
            }
            conn.close();
            return st;
        }catch(SQLException e){
        }
        return st;
    }
    
    public static synchronized ArrayList<SolvedTests> getGroupSolvedTests(int idGroup){
    	ArrayList<SolvedTests> st = new ArrayList<SolvedTests>();
        float tmpScore = 0;
        int score = 0;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT SolvedTests.* FROM SolvedTests, Group_Users WHERE SolvedTests.id_User = Group_Users.id_User AND Group_Users.id_Group = " + idGroup + " ORDER BY date DESC");
            while(rs.next()){SolvedTests tmp = new SolvedTests();
            	tmp.setId_SolvedTest(rs.getInt("id_SolvedTest"));
            	tmp.setTestName(getTestName(rs.getInt("id_Test")));
            	tmp.setId_User(rs.getInt("id_User"));
            	tmp.setUserName(UserDAO.getUserData(rs.getInt("id_User")).getFirst_name()+" "+UserDAO.getUserData(rs.getInt("id_User")).getLast_name());
            	tmpScore = rs.getFloat("score");
            	score = (int) (tmpScore * 100);
            	tmp.setScore(score);
            	tmp.setDate(rs.getString("date"));
            	st.add(tmp);
            }
            conn.close();
            return st;
        }catch(SQLException e){
        }
        return st;
    }
    
    public static synchronized SolvedTests getSolvedTestDetails(int idST){
    	SolvedTests st = new SolvedTests();
        int id = 0;
        float tmpScore = 0;
        int score = 0;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM SolvedTests WHERE id_SolvedTest = " + idST);
            if(rs.next()){
            	id = rs.getInt("id_SolvedTest");
            	st.setTestName(getTestName(rs.getInt("id_Test")));
            	st.setId_User(rs.getInt("id_User"));
            	tmpScore = rs.getFloat("score");
            	score = (int) (tmpScore * 100);
            	st.setScore(score);
            	st.setDate(rs.getString("date"));
            	st.setHistoryList(getSolvedTestHistory(id));
            }
            conn.close();
            return st;
        }catch(SQLException e){
        }
        return st;
    }
    
    public static synchronized List<SolvedTestsHistory> getSolvedTestHistory(int id){
        List<SolvedTestsHistory> s = new ArrayList<SolvedTestsHistory>();
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT * FROM SolvedTests_History WHERE id_SolvedTest =" + id);
            while(rs.next()){
            	SolvedTestsHistory a = new SolvedTestsHistory();
                a.setId_SolvedTest(rs.getInt("id_SolvedTest"));
                a.setQuestionName(getQuestionName(rs.getInt("id_Question")));
                a.setAnswerName(getAnswerName(rs.getInt("id_Answer")));
                s.add(a);
            }
            conn.close();
            return s;
        }catch(SQLException e){
        }
        return s;
    }
    
    public static synchronized String getQuestionName(int id){
        String i = null;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Question FROM Questions WHERE id_Question = " + id);
            if(rs.next()){
                i = rs.getString("Question");
            }
            conn.close();
        }catch(SQLException e){

        }
        return i;
    }
    
    public static synchronized String getAnswerName(int id){
        String i = null;
        try{
            getConnection();
            ResultSet rs = statement.executeQuery("SELECT Answer FROM Question_Answers WHERE id_Question_Answer = " + id);
            if(rs.next()){
                i = rs.getString("Answer");
            }
            conn.close();
        }catch(SQLException e){

        }
        return i;
    }
}