package engine.controller;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * Created by draxeer on 2017-01-21.
 */
public class ConnectionManager {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://drax1224.unixstorm.org/drax1224_base";
    static final String USER = "drax1224_base";
    static final String PASS = "drax122";

    public static Connection getConnection(){
        try {
            return DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String EncodeToUTF(String text) {
        byte textData[] = text.getBytes();

        try {
            return new String(textData, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
