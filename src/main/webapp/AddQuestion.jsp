<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Dodaj pytanie</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
		<link href="resources/css/form.css" rel="stylesheet" type="text/css">
		<script src="resources/javascript/form.js" type="text/javascript"></script>
        <script src="resources/javascript/mainFunctions.js" type="text/javascript"></script>
	</head>
	<body>
	<%@ include file="header.jsp" %>
	<nav>  
    <table>
      <%@ include file="actionButtons.jsp" %>
      <td style="text-align:center;">
        <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
          <h2>Utwórz pytanie dla testu <c:out value="${name}"/></h2>
          <button id="save" style="width:150px;" onclick="questionFunction()">Dodaj odpowiedź</button>
          <div align="center">
            <form action="addQuestion" method="post" id="myForm">
      		    <input type="hidden" name="id_test" value="${id}"></input>
      			  <input id="question" required type="text" name="question" placeholder="Pytanie"></input><br/>
      			  <input id="img" type="text" name="img" placeholder="Link do obrazka"></input><br/>
      			  <br/>
      			  <button id="save" type="submit">Utwórz</button><br/>
      		  </form>
          </div>
        </c:if>
        <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Edytor'}">
             <%@ include file="accessDenied.jsp" %>
        </c:if>
      </td>
    </table>
	</nav>
	<%@ include file="footer.jsp" %>
  </body>
</html>
