<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Członkowie grupy</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
        <tr>
          <%@ include file="actionButtons.jsp" %>
          <td style="text-align:center;">
          <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
            <c:if test="${!empty group.groupName}">
              <h1>#${group.id_Group} ${group.groupName}</h1>
            </c:if>
            <c:if test="${empty group.groupName}">
              <h1 style="color:red;">[INFO]: Brak grupy!</h1>
            </c:if>
            <c:if test="${!empty group.groupName}">
              <h3>Testy widoczne dla grupy</h3>
              <table class="accTable" align="center">
              	<tr class="accTR">
              		<th class="accTH">Nazwa testu</th>
              		<th class="accTH">Autor</th>
              		<th class="accTH">Usuń</th>
              	</tr>
              	<tr>
              		<c:forEach var="asd" items="${tests}">
  	                    <tr class="accTR">
		   			      	<td class="accTD"><c:out value="${asd.testName}"/></td>
				            <td class="accTD"><c:out value="${asd.author.first_name}"/> <c:out value="${asd.author.last_name}"/></td>
				            <td class="accTD">
                            	<a class="accManager" href="../Groups/removeTestFromGroup?idTest=<c:out value="${asd.id_Test}"/>&idGroup=<c:out value="${group.id_Group}"/>">
                            		<img src="${pageContext.request.contextPath}/resources/images/actions-delete-icon.png"/>
                            	</a>
                            </td>
  						 </tr>
            		</c:forEach>
              	</tr>
              </table>
              </br>
              <h3>Testy możliwe do dodania</h3>
              <table class="accTable" align="center">
              	<tr class="accTR">
              		<th class="accTH">Nazwa testu</th>
              		<th class="accTH">Autor</th>
              		<th class="accTH">Dodaj</th>
              	</tr>
              	<tr>
              		<c:forEach var="asd" items="${otherTests}">
  	                    <tr class="accTR">
  	            			<td class="accTD"><c:out value="${asd.testName}"/></td>
  							<td class="accTD"><c:out value="${asd.author.first_name}"/> <c:out value="${asd.author.last_name}"/></td>
  							<td class="accTD">
                  <a class="accManager" href="../Groups/addTestToGroup?idTest=<c:out value="${asd.id_Test}"/>&idGroup=<c:out value="${group.id_Group}"/>">
                    <img src="${pageContext.request.contextPath}/resources/images/actions-add-icon.png"/>
                  </a>
                </td>
  						</tr>
            			</c:forEach>
              	</tr>
              </table>
            </c:if>
            <c:if test="${empty group.groupName}">
              <p style="color:red;">
                [INFO]: Niestety, grupy, której szukasz nie ma w bazie.
              </p>
            </c:if>
            <br/>
            <br/>
            <div style="text-align:center;">
              <button	id="cancel"
                  type="submit"
                  onclick="goToGroupTests();">
                Cofnij
              </button>
            </div>
          </c:if>
          <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Edytor'}">
            <%@ include file="accessDenied.jsp" %>
          </c:if>
          </td>
        </tr>
    </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
