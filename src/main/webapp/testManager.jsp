<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Menedżer testów</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
  	<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td style="text-align:center;">
          <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
            <h1>Menedżer testów</h1>
            <!-- tworzenie testu -->
    		    <button id="createTest"
                    class="actionButton"
                    type="button"
                    onclick="goToCreateTest();">
                Stwórz test
            </button>
            <br/>
            <!-- wyświetlenie testów -->
    		    <button id="viewTests"
                    class="actionButton"
                    type="button"
                    onclick="goToViewTests();">
                Pokaż testy
            </button>
            <br/>
            <button id="viewTests"
                    class="actionButton"
                    type="button"
                    onclick="goToGroupTests();">
                Udostępnij testy
            </button>
          </c:if>
          <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
