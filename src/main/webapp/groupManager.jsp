<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Menedżer grup</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
  	<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td style="text-align:center;">
          <c:if test="${sessionScope.role == 'Admin'}">
            <h1>Menedżer grup</h1>
            <!-- tworzenie grupy -->
    		    <button id="createGroup"
                    class="actionButton"
                    type="button"
                    onclick="goToCreateGroup();">
                Stwórz grupę
            </button>
            <br/>
            <!-- wyświetlenie grup -->
    		    <button id="viewGroups"
                    class="actionButton"
                    type="button"
                    onclick="goToViewGroups();">
                Pokaż grupy
            </button>
          </c:if>
          <c:if test="${sessionScope.role != 'Admin'}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
