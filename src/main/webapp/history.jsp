<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<!DOCTYPE html>
<html>
	<head>
		<title>Historia</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                                <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
                    		<script>

                    		    $.urlParam = function (name) {
                                    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                                                  .exec(window.location.href);
                                    if(results){
                                    return results[1] || 0;
                                    }
                                    else{
                                        return -1;
                                    }
                                }
                    		    var add = $.urlParam('timeover');
                    		    if(add==="1"){
                    		    $.notify(
                                    "Skończył Ci się czas. Twoje wyniki zostały zapisane",
                                    { position:"left top", className: "info" }
                                    );
                                }
                            </script>
		<nav>
      <table>
        <tr>
        <%@ include file="actionButtons.jsp" %>
        <td>
        	<c:if test="${sessionScope.role != null}">
        		<h1 style="text-align:center;">Historia wyników</h1>
       			<table class="accTable" align="center">
					<tr>
						<th class="accTH" style="width: 5%">ID</th>
						<th class="accTH" style="width: 15%">Nazwa testu</th>
						<th class="accTH" style="width: 10%">Wynik</th>
						<th class="accTH" style="width: 35%">Data rozwiązania</th>
						<th class="accTH" style="width: 35%">Odpowiedzi</th>
					</tr>
					<%
						int id = 0;
					%>
					<c:forEach var="history" items="${history}">
						<tr class="accTR">
							<td><%=id+1%></td>
							<td class="accTD">
								<c:if test="${empty history.testName}">
									Usunięto
								</c:if>
								<c:if test="${!empty history.testName}">
									<c:out value="${history.testName}"/>
								</c:if>
							</td>
							<td class="accTD"><c:out value="${history.score}%"/></td>
							<td class="accTD"><c:out value="${history.date}"/></td>
							<td class="accTD" align="center">
								<button id="save" onclick="location.href='HistoryDetails?id=<c:out value="${history.id_SolvedTest}"/>'">Szczegóły</button>
							</td>
						</tr>
						<%
							id++;
						%>
					</c:forEach>
				</table>
	            <div style="text-align:center;">
		   			<button id="cancel"
				        type="submit"
				        onclick="goToUserPanel();">
				      Cofnij
				    </button>
	            </div>
      		<br/>
        	</c:if>
	        <c:if test="${sessionScope.role == null}">
	             <%@ include file="accessDenied.jsp" %>
	        </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
