<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Dynamic Form</title>
        <link href="resources/css/form.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main_content">
            <div class="first">
                <p>Dynamic Form</p>
            </div>
            <div class="two">
                <h4>Frequently Used Form Fields</h4>
                <button onclick="questionFunction()">Question</button>
                <button onclick="resetElements()">Reset</button>
                <button id="cancel" type="submit" onclick="backToHomePage();">Back</button>
            </div>
            <div class="three">
                <h2>Your Dynamic Form!</h2>
                <form action="#" id="mainform" method="get" name="mainform">
                    <span id="myForm"></span>
                    <p></p><input type="submit" onclick="submitJSON();" value="Submit">
                </form>
            </div>
            <div class="four">
                <p>2016 ©All rights reserved.</p>
            </div>
        </div>
        <script src="resources/javascript/form.js" type="text/javascript"></script>
        <script src="resources/javascript/mainFunctions.js" type="text/javascript"></script>
    </body>
</html>
