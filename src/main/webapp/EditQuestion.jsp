<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Edytuj pytanie</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
        <table>
          <%@ include file="actionButtons.jsp" %>
          <td>
            <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
            <h2 style="text-align:center;">Edycja pytania</h2>
          		<form action="${pageContext.request.contextPath}/Tests/editQuestion" method="post">
          			<label for="question_name">Treść pytania</label><br/>
          			<input id="question_name" required value="<c:out value="${question.question}"/>" type="text" name="question_name"><br/>
          			<label for="img">Link do obrazka </label><br/>
          			<input id="img" value="<c:out value="${question.img}"/>" type="text" name="img"><br/>
                <br/>
          			<input type="hidden" id="id" name="id" value="${id}">
          			<input type="hidden" id="idTest" name="idTest" value="${idTest}">
          			<button id="save" type="submit">Edytuj</button>
   			        <button	id="cancel" type="button" onclick="window.history.back();">Cofnij</button>
          		</form>
            </c:if>
            <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
                 <%@ include file="accessDenied.jsp" %>
            </c:if>
          </td>
        </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
