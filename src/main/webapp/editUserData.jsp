<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page import="engine.controller.UserDAO" %>
<%@ page import="engine.model.User" %>

<%
    int id = Integer.parseInt(request.getParameter("idUser"));
    User user = UserDAO.getUserData(id);
    pageContext.setAttribute("user", user);
%>

<html>
	<head>
		<title>Edycja użytkownika</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td>
            <c:if test="${sessionScope.idUser == user.id_User}">
    			<h2 style="text-align:center;">Edycja użytkownika</h2>
    				<form action="accManager" method="post">
    					<table class="accTable" align="center">
    						<tr class="accTR">
    							<th class="accTH">Atrybut</th>
    							<th class="accTH">Wartość</th>
    						</tr>
    						<tr class="accTR">
    							<td class="accTD"><label>Imię</label></td>
    							<td class="accTD">
    								<input	id="text1"
    										type="text"
    										name="first_name"
    										value="<%=user.getFirst_name()%>"
    										pattern="([A-ZĄĆĘŁŃÓŚŻŹ]{1}[a-ząćęłńóśżź]*.{1,20})"
    										title="Pierwsza litera duża, reszta małe, max 20."
    										required>
    							</td>
    						</tr>
    						<tr class="accTR">
    							<td class="accTD"><label>Nazwisko</label></td>
    							<td class="accTD">
    								<input	id="text2"
    										type="text"
    										name="last_name"
    										value="<%=user.getLast_name()%>"
    										pattern="([A-ZĄĆĘŁŃÓŚŻŹ]{1}[a-ząćęłńóśżź]*.{1,20})"
    										title="Pierwsza litera duża, reszta małe, max 20."
    										required>
    							</td>
    						</tr>
    						<tr class="accTR">
    							<td class="accTD"><label>Indeks studenta</label></td>
    							<td class="accTD">
    								<input	id="text5"
    										type="text"
    										name="st_index"
    										value="<%=user.getSt_index()%>"
    										pattern="[0-9]{6}"
    										title="Sześć cyfr."
    										required>
    							</td>
    						</tr>
    						<input id="text6" type="hidden" name="id_User" value="<%=user.getId_User()%>">
    						<input id="text7" type="hidden" name="oldPass" value="<%=user.getPassw()%>">
                            <input type="hidden" name="email" value="<%=user.getEmail()%>">
                            <input type="hidden" name="passw" value="<%=user.getPassw()%>">
                            <input type="hidden" name="id_Role" value="<%=user.getId_Role()%>">
    					</table>
    					<br/>
    					<button id="save" type="submit">Zapisz</button><br/>
    					<span class="error">${error}</span>
    				</form>
                <div style="text-align:center;">
                    <button id="cancel" type="submit" onclick="goToUserPanel();">Anuluj</button>
                </div>
            </c:if>
            <c:if test="${sessionScope.idUser != user.id_User}">
                <%@ include file="accessDenied.jsp" %>
            </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
