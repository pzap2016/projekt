<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page import="engine.controller.UserDAO" %>
<%@ page import="engine.model.User" %>

<%
    int id = Integer.parseInt(request.getParameter("idUser"));
    User user = UserDAO.getUserData(id);
    pageContext.setAttribute("user", user);
%>

<html>
	<head>
		<title>Zmiana hasła</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td>
            <c:if test="${sessionScope.role != null}">
    			<h2 style="text-align:center;">Zmiana hasła</h2>
    				<form action="changePassword" method="post">
    					<table class="accTable" align="center">
    						<tr class="accTR">
    							<td class="accTD"><label>Stare hasło</label></td>
    							<td class="accTD">
    								<input	id="text4"
    										type="password"
    										name="passw"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
    										value=""
    										title="Co najmniej jedna liczba, jedna duża lub mała litera, minimum 8 znaków."
    										required>
    							</td>
    						</tr>
                            <tr class="accTR">
                                <td class="accTD"><label>Nowe hasło</label></td>
                                <td class="accTD">
                                    <input  id="text4"
                                            type="password"
                                            name="passw_new"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
                                            value=""
                                            title="Co najmniej jedna liczba, jedna duża lub mała litera, minimum 8 znaków."
                                            required>
                                </td>
                            </tr>
                            <tr class="accTR">
                                <td class="accTD"><label>Powtórz nowe hasło</label></td>
                                <td class="accTD">
                                    <input  id="text5"
                                            type="password"
                                            name="passw_confirm"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
                                            value=""
                                            title="Hasła muszą być identyczne."
                                            required>
                                </td>
                            </tr>
                            <input name="email" type="hidden" value="<%=user.getEmail()%>">
                            <input name="last_name" type="hidden" value="<%=user.getLast_name()%>">
                            <input name="first_name" type="hidden" value="<%=user.getFirst_name()%>">
                            <input name="id_Role" type="hidden" value="<%=user.getId_Role()%>">
    						<input id="text6" type="hidden" name="id_User" value="<%=user.getId_User()%>">
    						<input id="text7" type="hidden" name="oldPass" value="<%=user.getPassw()%>">
                            <input name="st_index" type="hidden" value="<%=user.getSt_index()%>">
    					</table>
    					<br/>
                        <span class="error">${error}</span>
                        <br/>
    					<button id="save" type="submit">Zmień</button>
    				</form>
            <div style="text-align:center;">
                <button id="cancel" type="submit" onclick="goToUserPanel();">Anuluj</button>
            </div>
            </c:if>
            <c:if test="${sessionScope.role == null}">
                <%@ include file="accessDenied.jsp" %>
            </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
