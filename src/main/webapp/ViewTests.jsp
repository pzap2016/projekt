<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<head>
		<title>Testy</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
            				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                            <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
                		<script>

                		    $.urlParam = function (name) {
                                var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                                              .exec(window.location.href);
                                if(results){
                                return results[1] || 0;
                                }
                                else{
                                    return -1;
                                }
                            }
                		    var rem = $.urlParam('succesR');
                		    var add = $.urlParam('succesA');
                		    if(add==="1"){
                		    $.notify(
                                "Pomyślnie utworzono nowy test.",
                                { position:"left top", className: "info" }
                                );
                            }
                            if(rem==="1"){
                                $.notify(
                                    "Test usunięto pomyślnie",
                                     { position:"left top", className: "info" }
                                 );
                            }
                        </script>
		<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td>
            <c:if test="${sessionScope.role != null}">
    			<h1 style="text-align:center;">Testy</h1>
    			<table class="accTable" align="center">
    				<c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
    				<tr>
    					<th class="accTH">Nazwa testu</th>
    					<th class="accTH">Autor</th>
    					<th class="accTH" colspan="4">Akcje</th>
    				</tr>
    				</c:if>
    				<c:if test="${(sessionScope.role == 'User')}">
    				   <tr>
                        					<th class="accTH">Nazwa testu</th>
                        					<th class="accTH">Ilość pytań</th>
                        					<th class="accTH">Ilość czasu</th>
                        					<th class="accTH">Rozpocznij test</th>
                       </tr>
    				</c:if>
    	<c:forEach var="asd" items="${tests}">
    	    <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
    			<tr class="accTR">
					<td class="accTD"><c:out value="${asd.testName}"/></td>
					<td class="accTD"><c:out value="${asd.author.first_name} ${asd.author.last_name}"/></td>
                    <c:if test="${sessionScope.role == 'Admin'}">
		   			<td class="accTD">
	                	<a class="accManager" href="Tests/Editors?id=<c:out value="${asd.id_Test}"/>" title="Edytorzy testu">
	                		<img src="${pageContext.request.contextPath}/resources/images/editors2.png"/>
	               		</a>
	               	</td>
                    </c:if>
					<td class="accTD">
		            	<a class="accManager" href="Tests/Details?id=<c:out value="${asd.id_Test}"/>" title="Zobacz szczegóły">
		            		<img src="${pageContext.request.contextPath}/resources/images/info.png"/>
		            	</a>
		            </td>
		   			<td class="accTD">
		            	<a class="accManager" href="removeTest?id=<c:out value="${asd.id_Test}"/>" title="Usuń test">
		            		<img src="${pageContext.request.contextPath}/resources/images/trash-icon.png"/>
		            	</a>
		            </td>
                </tr>
            </c:if>
            <c:if test="${(sessionScope.role == 'User')}">
            <tr class="accTR">
                <td class="accTD"><c:out value="${asd.testName}"/></td>
                <td class="accTD"><c:out value="${asd.questions_per_test}"/></td>
                <td class="accTD"><c:out value="${asd.time_per_test}"/></td>
                <td class="accTD">
                    						<button id="startTest"
                    										class="actionButton"
                    										type="button"
                    										onclick="startTest(${asd.id_Test});">
                    								Rozpocznij
                    						</button>
                    </td>
            </tr>
            </c:if>
    	</c:forEach>
    	</table>
    	    <br/>
                <center><b><span id="error" class="error">${error}</span></b></center>
            <br/>
	    <div style="text-align:center;">
        <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
			<button	id="cancel"
					type="submit"
					onclick="goToTestManager();">
				Cofnij
			</button>
        </c:if>
        <c:if test="${sessionScope.role == 'User'}">
            <button id="cancel"
                    type="submit"
                    onclick="goToUserPanel();">
                Cofnij
            </button>
        </c:if>
          </div>
          </c:if>
          <c:if test="${sessionScope.role == null}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
