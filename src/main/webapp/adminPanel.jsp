<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Panel administracyjny</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
  	<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td style="text-align:center;">
          <c:if test="${sessionScope.role == 'Admin'}">
            <h1>Panel administracyjny</h1>
            <!-- menedżer użytkowników -->
    		    <button id="accManager"
                    class="actionButton"
                    type="button"
                    onclick="goToAccManager();">
                Zarządzaj kontami
            </button>
            <!-- menedżer testów -->
    		    <button id="testManager"
                    class="actionButton"
                    type="button"
                    onclick="goToTestManager();">
                Zarządzaj testami
            </button>
            <!-- menedżer grup -->
    		    <button id="groupManager"
                    class="actionButton"
                    type="button"
                    onclick="goToGroupManager();">
                Zarządzaj grupami
            </button>
          </c:if>
          <c:if test="${sessionScope.role != 'Admin'}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
