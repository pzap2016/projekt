<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Grupy</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td>
        <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
    			<h1 style="text-align:center;">Grupy</h1>
    			<table class="accTable" align="center">
    				<tr>
    					<th class="accTH" style="width: 70%;">Nazwa grupy</th>
    					<th class="accTH" style="width: 30%;">Udostępnianie</th>
    				</tr>
    				<c:forEach var="asd" items="${groups}">
    					<tr class="accTR">
    						<td class="accTD"><c:out value="${asd.groupName}"/></td>
			                <td class="accTD" align="center">
								<button id="edit" style="background-color:brown; color:white;" onclick="location.href='/PZ2016/Tests/groupTests?id=<c:out value="${asd.id_Group}"/>'">Wybierz</button>
							</td>
    					</tr>
    				</c:forEach>
    			</table>
    			<br/>
          <div style="text-align:center;">
              <button id="cancel"
                    type="submit"
                    onclick="goToTestManager();">
                  Cofnij
                </button>
              </div>
            <br/>
          </c:if>
          <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Edytor'}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
