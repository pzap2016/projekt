<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
	<head>
		<title>Zarejestruj się</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<p id="headerButtons"></p>
		<br/>
		<nav>
			<h2>Zarejestruj się!</h2>
			<form action="Register" method="post">
				<input id="text1" type="text" name="first_name"
						pattern="([A-ZĄĆĘŁŃÓŚŻŹ]{1}[a-ząćęłńóśżź]*.{1,20})"
            			placeholder="np. Jan" required
						title="Pierwsza litera duża, reszta małe, max 20.">
						<br/>
				<label>Imię</label><br/>
				<input id="text2" type="text" name="last_name"
						pattern="([A-ZĄĆĘŁŃÓŚŻŹ]{1}[a-ząćęłńóśżź]*.{1,20})"
            			placeholder="np. Kowalski" required
						title="Pierwsza litera duża, reszta małe, max 20.">
						<br/>
				<label>Nazwisko</label><br/>
				<input id="text3" type="text" name="email"
						pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
            			placeholder="np. jan.kowalski@wp.pl"
						title="Adres e-mail." required>
						<br/>
				<label>E-mail</label><br/>
				<input id="text4" type="password" name="passw"
						pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
            			placeholder="********"
						title="Co najmniej jedna liczba, jedna duża lub mała litera, minimum 8 znaków."
						required>
						<br/>
				<label>Hasło</label><br/>
				<input id="text6" type="password" name="passw_confirmation"
						pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
            			placeholder="********"
						title="Hasła muszą być identyczne."
						required>
						<br/>
				<label>Powtórz hasło</label><br/>
				<input id="text5" type="text" name="st_index"
						pattern="[0-9]{6}"
            			placeholder="np. 150129"
						title="Sześć cyfr." required>
						<br/>
				<label>Indeks studenta</label><br/>
				<br/>
				<input id="register" type="submit" value="Zarejestruj"><br/>
				<span class="error">${error}</span>
			</form>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>