<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<!DOCTYPE html>
<html>
	<head>
		<title>Szczegóły testu</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
    				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                    <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
        		<script>

        		    $.urlParam = function (name) {
                        var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                                      .exec(window.location.href);
                        if(results){
                        return results[1] || 0;
                        }
                        else{
                            return -1;
                        }
                    }
        		    var add = $.urlParam('succesad');
        		    var delP = $.urlParam('succes');
        		    var delO = $.urlParam('succesA');
        		    var dA = $.urlParam('succesadd');
        		    if(add==="1"){
        		    $.notify(
                        "Pytanie dodano pomyślnie.",
                        { position:"left top", className: "info" }
                        );
                    }
                    if(delP==="1"){
                        $.notify(
                            "Pytanie usunięto pomyślnie.",
                             { position:"left top", className: "info" }
                         );
                    }
                    if(delO==="1"){
                        $.notify(
                            "Odpowiedź usunięto pomyślnie.",
                             { position:"left top", className: "info" }
                         );
                    }
                    if(dA==="1"){
                        $.notify(
                            "Odpowiedź/i dodano pomyślnie.",
                             { position:"left top", className: "info" }
                         );
                    }
                </script>
		<nav>
      <table>
        <tr>
        <%@ include file="actionButtons.jsp" %>
        <td>
          <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
          <h1 style="text-align:center;">Szczegóły testu</h1><br/>
          <table class="accTable" align="center" style="width:600px">
      				<tr class="accTR">
      					<th class="accTH">Nazwa testu</th>
      					<th class="accTH">Ilość pytań na test</th>
      					<th class="accTH">Ilość czasu (min)</th>
      					<th class="accTH">Edytuj</th>
      					<th class="accTH">Dodaj nowe pytanie</th>
      				</tr>
                    <tr class="accTR">
                    <td> <c:out value="${test.testName}"/> </td>
                    <td> <c:out value="${test.questions_per_test}"/> </td>
                    <td> <c:out value="${test.time_per_test}"/> </td>
                    <td>                   <a href="${pageContext.request.contextPath}/Tests/Edit?id=<c:out value="${test.id_Test}"/>" title="Edytuj szczegóły testu">
                                             <img src="${pageContext.request.contextPath}/resources/images/actions-edit-icon.png"/>
                                           </a> </td>
                    <td>
                    <a href="${pageContext.request.contextPath}/addQuestion?id=<c:out value="${test.id_Test}"/>" title="Dodaj nowe pytanie">
                                                                 <img src="${pageContext.request.contextPath}/resources/images/addQ2.png"/>
                                                               </a> </td>
                    </td>
                    </tr>
          </table>
      			<br/><br/>
      			<table class="accTable" align="center" style="width:600px">
      				<tr class="accTR">
      					<th class="accTH">ID</th>
      					<th class="accTH">Pytanie</th>
      					<th class="accTH">Odpowiedzi</th>
      					<th class="accTH">Edytuj</th>
      					<th class="accTH">Usuń</th>
      					<th class="accTH">Dodaj odpowiedzi</th>
      				</tr>
      				<%
      					int id = 0;
      				%>
              <c:if test="${empty test.questionList}">
                <tr>
                  <td></td>
                  <td colspan="3" style="color:red; font-size:smaller;">
                    [INFO]: Niniejszy test jest pusty. Dodaj co najmniej
                    jedno pytanie do tego testu, aby je tutaj zobaczyć.
                  </td>
                </tr>
              </c:if>
      				<c:forEach var="asd" items="${test.questionList}">
      					<tr class="accTR">
      						<td><%=id+1%></td>
      						<td id="td<%=id%>" class="accTD, question<%=id%>" style="text-align:justify; ">
      							<c:out value="${asd.question}"/>
      						</td>
      						<input type="hidden" id="idd<%=id%>" value="${asd.id_Question}"/>
      						<td class="accTD" align="center">
      							<button id="save<%=id%>" onClick="visibility('answer<%=id%>');">
      								Rozwiń/Zwiń
      							</button>
      						</td>
      						<td class="accTD" align="center">
      						                                     <a href="${pageContext.request.contextPath}/Tests/editQuestion?id=<c:out value="${asd.id_Question}"/>&idTest=<c:out value="${test.id_Test}"/>" title="Edytuj pytanie" >
                                                                              <img src="${pageContext.request.contextPath}/resources/images/edit.png"/>
                                                                 </a>
      						</td>
      						<td class="accTD" align="center">
                                <a href="${pageContext.request.contextPath}/removeQuestion?idQuestion=<c:out value="${asd.id_Question}"/>&idtest=<c:out value="${test.id_Test}"/>" title="Usuń pytanie">
                                             <img src="${pageContext.request.contextPath}/resources/images/trash-icon.png"/>
                                </a>
      						</td>
      						<td>
                                 <a href="${pageContext.request.contextPath}/addAnswers?idQuestion=<c:out value="${asd.id_Question}"/>&idtest=<c:out value="${test.id_Test}"/>" title="Dodaj nową odpowiedź/i" >
                                              <img src="${pageContext.request.contextPath}/resources/images/addA2.png"/>
                                 </a>
      						</td>
                </tr>
      					<c:if test="${empty asd.answerList}">
      							<tr style="color:red;background-color:white;">
      								<td/>
      								<td colspan="5">
      									<p align="justify" style="font-size:smaller;">
      										[INFO]: Powyższe pytanie nie zawiera dopasowanych odpowiedzi.
      										Dodaj je, aby mogły zostać wyświetlone. <br/>
      										Pytanie nie zostanie uwględnione, przy losowaniu puli pytań.
      									</p>
      								</td>
      							</tr>
      						</c:if>

      						<c:forEach var="ans" items="${asd.answerList}">
      							<tr class="accTR, answer<%=id%>" style="display: none;">
                                <td> </td>
      								<td	id="answer<%=id%>" class="accTD"
      									style="display:block; margin-left:25px; text-align:justify;">
      									<li>${ans.getAnswer()}</li>
      								</td>
      								<td class="accTD" align="center">
      									<c:choose>
      										<c:when test="${ans.getIs_Correct()}">
      											 <img src="${pageContext.request.contextPath}/resources/images/true2.png"/>
      										</c:when>
      										<c:otherwise>
      											 <img src="${pageContext.request.contextPath}/resources/images/false3.png"/>
      										</c:otherwise>
      									</c:choose>
      								</td>
      								<td class="accTD" align="center">
      								                                     <a href="${pageContext.request.contextPath}/Tests/editAnswer?id=<c:out value="${ans.id_Answer}"/>&idTest=<c:out value="${idTest}"/>" title="Edytuj odpowiedź" >
                                                                                      <img src="${pageContext.request.contextPath}/resources/images/edit2.png"/>
                                                                         </a>
      								</td>
      								<td class="accTD" align="center">
                                        <a href="${pageContext.request.contextPath}/removeAnswer?idAnswer=<c:out value="${ans.id_Answer}"/>&idtest=<c:out value="${test.id_Test}"/>" title="Usuń odpowiedź">
                                          <img src="${pageContext.request.contextPath}/resources/images/trash-icon.png"/>
                                        </a>
      								</td>
      								</tr>
      						</c:forEach>
      						<%
      							id++;
      						%>

      				        </c:forEach>
              <tr/>
      			</table>
      			<br/>
            <div style="text-align:center;">
        			<button	id="cancel"
        					type="submit"
        					onclick="goToViewTests();">
        				Cofnij
        			</button>
            </div>
      			<br/>
          </c:if>
          <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
