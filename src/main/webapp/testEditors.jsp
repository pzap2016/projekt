<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>testEditors</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
        <tr>
          <%@ include file="actionButtons.jsp" %>
          <td style="text-align:center;">
          <c:if test="${sessionScope.role == 'Admin'}">
            <c:if test="${!empty test.testName}">
              <h1>#${test.id_Test} ${test.testName}</h1>
            </c:if>
            <c:if test="${empty test.testName}">
              <h1 style="color:red;">[INFO]: Brak testu!</h1>
            </c:if>
            <c:if test="${!empty test.testName}">
              <h3>Edytorzy</h3>
              <table class="accTable" align="center">
              	<tr class="accTR">
              		<th class="accTH">Imię</th>
              		<th class="accTH">Nazwisko</th>
              		<th class="accTH">Usuń</th>
              	</tr>
              	<tr>
              		<c:forEach var="asd" items="${editors}">
  	                    <tr class="accTR">
  	            			       <td class="accTD"><c:out value="${asd.first_name}"/></td>
  							             <td class="accTD"><c:out value="${asd.last_name}"/></td>
  							             <td class="accTD">
                               <a class="accManager" href="removeEditorFromTest?idUser=<c:out value="${asd.id_User}"/>&idTest=<c:out value="${test.id_Test}"/>">
                                <img src="${pageContext.request.contextPath}/resources/images/actions-delete-icon.png"/>
                              </a>
                            </td>
  						          </tr>
            			</c:forEach>
              	</tr>
              </table>
              <br/>
              <h3>Edytorzy możliwi do dodania</h3>
              <table class="accTable" align="center">
              	<tr class="accTR">
              		<th class="accTH">Imię</th>
              		<th class="accTH">Nazwisko</th>
              		<th class="accTH">Dodaj</th>
              	</tr>
              	<tr>
              		<c:forEach var="asd" items="${otherUsers}">
  	                    <tr class="accTR">
  	            			<td class="accTD"><c:out value="${asd.first_name}"/></td>
  							<td class="accTD"><c:out value="${asd.last_name}"/></td>
  							<td class="accTD">
                  <a class="accManager" href="addEditorToTest?idUser=<c:out value="${asd.id_User}"/>&idTest=<c:out value="${test.id_Test}"/>">
                    <img src="${pageContext.request.contextPath}/resources/images/actions-add-icon.png"/>
                  </a>
                </td>
  						</tr>
            			</c:forEach>
              	</tr>
              </table>
            </c:if>
            <c:if test="${empty test.testName}">
              <p style="color:red;">
                [INFO]: Niestety, test, którego szukasz, nie ma w bazie.
              </p>
            </c:if>
            <br/>
            <br/>
            </c:if>
            <c:if test="${sessionScope.role != 'Admin'}">
              <%@ include file="accessDenied.jsp" %>
            </c:if>
            <div style="text-align:center;">
              <button	id="cancel"
                  type="submit"
                  onclick="goToViewTests();">
                Cofnij
              </button>
            </div>
          </td>
        </tr>
    </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
