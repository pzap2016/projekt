<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>testEdit</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<script>
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    	    if (charCode > 31 && (charCode < 48 || charCode > 57))
		    	        return false;
		    	    return true;
		    	}
		</script>
		<nav>
      <table>
        <tr>
          <%@ include file="actionButtons.jsp" %>
          <td style="text-align:center;">
          <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
            <c:if test="${!empty test.testName}">
              <h1>#${test.id_Test} ${test.testName}</h1>
            </c:if>
            <c:if test="${empty test.testName}">
              <h1 style="color:red;">[INFO]: Brak testu!</h1>
            </c:if>
            <c:if test="${!empty test.testName}">
            	<h2 style="text-align:center;">Edycja testu</h2>
          		<form action="${pageContext.request.contextPath}/Tests/Edit" method="post">
          			<label for="test_name">Nazwa testu</label><br/>
          			<input id="test_name" required value="<c:out value="${test.testName}"/>" type="text" name="test_name"><br/><br/>
          			<label for="test_amount">Ilość pytań przy wykonywaniu testu</label><br/>
          			<input id="test_amount" required min="1" value="<c:out value="${test.questions_per_test}"/>" type="text" onkeypress="return isNumberKey(event)" name="test_amount"><br/><br/>
          			<label for="test_time">Czas na wykonanie testu (min)</label><br/>
          			<input id="test_time" required min="1" value="<c:out value="${test.time_per_test}"/>" type="text" onkeypress="return isNumberKey(event)" name="test_time"><br/><br/>
          			<input type="hidden" id="idTest" name="idTest" value="${idTest}">
          			<button id="save" type="submit">Edytuj</button>
   			        <button	id="cancel" type="button" onclick="window.history.back();">Cofnij</button>
          		</form>
            <c:if test="${empty test.testName}">
              <p style="color:red;">
                [INFO]: Niestety, testu którego szukasz nie ma w bazie.
              </p>
            </c:if>
            </c:if>
            </c:if>
            <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
                 <%@ include file="accessDenied.jsp" %>
            </c:if>
          </td>
        </tr>
    </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
