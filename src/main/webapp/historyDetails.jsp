<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<!DOCTYPE html>
<html>
	<head>
		<title>Szczegóły rozwiązania</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<nav>
      <table>
        <tr>
        <%@ include file="actionButtons.jsp" %>
        <td>
        	<c:if test="${sessionScope.role != null}">
        		<h1 style="text-align:center;">Szczegóły rozwiązania</h1>
       			<table class="accTable" align="center">
					<tr>
						<th class="accTH">Nazwa testu</th>
						<th class="accTH">Wynik</th>
						<th class="accTH">Data rozwiązania</th>
						<th class="accTH">Użytkownik</th>
					</tr>
					<tr class="accTR">
						<td class="accTD">
							<c:if test="${empty history.testName}">
								Usunięto
							</c:if>
							<c:if test="${!empty history.testName}">
								<c:out value="${history.testName}"/>
							</c:if>
						</td>
						<td class="accTD"><c:out value="${history.score}%"/></td>
						<td class="accTD"><c:out value="${history.date}"/></td>
						<td class="accTD"><c:out value="${user.first_name} ${user.last_name}"></c:out></td>
					</tr>
				</table></br></br>
				<table class="accTable" align="center">
					<tr>
						<th class="accTH">Pytanie</th>
						<th class="accTH">Odpowiedź</th>
					</tr>
					<c:forEach var="ans" items="${history.historyList}">
						<tr class="accTR">
							<td class="accTD" align="center">
								<c:out value="${ans.questionName}"/>
							</td>
							<td class="accTD" align="center">
								<c:out value="${ans.answerName}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
	            <div style="text-align:center;">
		   			<button id="cancel"
				        type="submit"
				        onclick="window.history.back();">
				      Cofnij
				    </button>
	            </div>
      		<br/>
        	</c:if>
	        <c:if test="${sessionScope.role == null}">
	             <%@ include file="accessDenied.jsp" %>
	        </c:if>
        </td>
      </table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
