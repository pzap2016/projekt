<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<p style="text-align:center;">
  <b>Nieupoważnionym wstęp wzbroniony!</b><br/>
  Obawiam się, że nie masz odpowiednich uprawnień,
  aby oglądać zawartość tej strony.
</p>
