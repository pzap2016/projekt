<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Wykonaj test</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/clock.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/lightbox.min.css">
		<style>
        ul {
            list-style-type: none;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-content: space-between;
            flex-direction: row;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #999999;
            border: solid 2px darkgray;
            border-style: outset;
        }

        li {
            float: left;

        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        li a:hover:not(.active) {
            background-color: #111;

        }

        .active {
            background-color: #4CAF50;
        }
        .btn-link{
            width: 40px;
        }
        .btn-link-a{
            color: red;
            width: 40px;
        }
        .btn-link-a-a{
             width: 40px;
             color: green;
             background-color: red;
        }
        .btn-link-a-n{
             width: 40px;
             color: red;
             background-color: green;
        }
        #ImageContainerr img { max-width: 200px; height: auto }
        </style>
        <script>
            function getTimeRemaining(endtime) {
              var t = Date.parse(endtime) - Date.parse(new Date());
              var seconds = Math.floor((t / 1000) % 60);
              var minutes = Math.floor((t / 1000 / 60) % 60);
              var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
              var days = Math.floor(t / (1000 * 60 * 60 * 24));
              return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
              };
            }

            function initializeClock(id, endtime) {
              var clock = document.getElementById(id);
              var daysSpan = clock.querySelector('.days');
              var hoursSpan = clock.querySelector('.hours');
              var minutesSpan = clock.querySelector('.minutes');
              var secondsSpan = clock.querySelector('.seconds');

              function updateClock() {
                var t = getTimeRemaining(endtime);
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                  clearInterval(timeinterval);
                  document.location.href = '${pageContext.request.contextPath}/End?timeover=1';
                }
              }

              updateClock();
              var timeinterval = setInterval(updateClock, 1000);
            }
        window.onload = function () {
        var deadline = new Date(Date.parse(document.getElementById('ce').getAttribute('atrybut')));
        initializeClock('clockdiv', deadline);
        };
        </script>

	</head>
	<body>
	<div id="ce" atrybut="${sessionScope.time}" style="display:none;"></div>
	<%@ include file="header.jsp" %>
	<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
	<script src="${pageContext.request.contextPath}/resources/javascript/lightbox-plus-jquery.min.js"></script>

    <br/>
    <nav>
    <table>
    <tr>
    <%@ include file="actionButtons.jsp" %>
    <td ></td>
    <ul>

    <c:forEach var="dda" items="${sessionScope.qpool}" varStatus="i">
            <c:set var="contains" value="false" />
                <c:forEach var="item" items="${sessionScope.answers}">
                  <c:if test="${item.id_Question eq dda.id_Question}">
                    <c:set var="contains" value="true" />
                  </c:if>
                </c:forEach>
    <li>
    <form action="${pageContext.request.contextPath}/Solve" method="post">
        <input type="hidden" value="${i.index}" name="index"/>
    <c:if test="${contains}">

        <c:if test="${(sessionScope.index == i.index )}">

            <button type="submit" name="index" value="${i.index}" class="btn-link-a-a"> <c:out value="${i.count}"/> </button>
        </c:if>
        <c:if test="${(sessionScope.index != i.index )}">

            <button type="submit" name="index" value="${i.index}" class="btn-link-a-n"> <c:out value="${i.count}"/> </button>
           </c:if>
     </c:if>

     <c:if test="${contains ne true}">
             <c:if test="${(sessionScope.index == i.index )}">

                 <button type="submit" name="index" value="${i.index}" class="btn-link-a-a"> <c:out value="${i.count}"/> </button>
             </c:if>
             <c:if test="${(sessionScope.index != i.index )}">

                 <button type="submit" name="index" value="${i.index}" class="btn-link"> <c:out value="${i.count}"/> </button>
                </c:if>
     </c:if>
    </form>

    </li>
    </c:forEach>
    </ul>
    <td class="accTD"> Pytanie:  ${sessionScope.qpool.get(sessionScope.index).question} <br/> <br/>
      <c:if test="${!empty sessionScope.qpool.get(sessionScope.index).img}">
      <div id="ImageContainerr">
       <center> <a class="example-image-link" href=${sessionScope.qpool.get(sessionScope.index).img} data-lightbox="example-1"><img class="example-image" src=${sessionScope.qpool.get(sessionScope.index).img} alt="image-1" /></a>
        </center>
      </div>
      </c:if>
      <br/>
      <form action="${pageContext.request.contextPath}/Solve" method="post">
      <br/><br/>

        <input type="hidden" name="idQuestion" value="${sessionScope.qpool.get(sessionScope.index).id_Question}">
            <c:forEach var="asd" items="${sessionScope.qpool.get(sessionScope.index).answerList}" varStatus="i">
                    <c:set var="contains" value="false" />
                    <c:set var="in"/>
                    <c:set var="q" value="${sessionScope.qpool.get(sessionScope.index)}"/>
                            <c:forEach var="item" items="${sessionScope.answers}" varStatus="l">
                              <c:if test="${item.id_Question eq q.id_Question}">
                                <c:set var="in" value="${l.index}"/>
                                <c:set var="contains" value="true" />
                              </c:if>
                            </c:forEach>
                            <c:set var="contains2" value="false" />
                            <c:forEach var="a" items="${sessionScope.answers.get(in).answerList}">
                             <c:if test="${a.id_Answer eq asd.id_Answer}">
                                      <c:set var="contains2" value="true" />
                             </c:if>
                            </c:forEach>
            <c:if test="${contains ne true}">
                <section title=".squaredTwo">
                <div class="squaredTwo">
                <input type="checkbox" name="answers" value="${asd.id_Answer}">
                <label><c:out value="${asd.answer}"/></label>
                </div>
                </section>
                <br/>
            </c:if>
            <c:if test="${contains}">
                <c:if test="${contains2}">
                <input type="checkbox" name="answers" value="${asd.id_Answer}" checked>
                <label><c:out value="${asd.answer}"/></label>
                <br/>
                </c:if>
                <c:if test="${contains2 ne true}">
                  <input type="checkbox" name="answers" value="${asd.id_Answer}">
                   <label><c:out value="${asd.answer}"/></label>
                   <br/>
                </c:if>
            </c:if>
            </c:forEach><br/><br/>
        <button id="save" type="submit">Zapisz</button>
       </form>

       <h1>Pozostało:</h1>
       <div id="clockdiv">
         <div>
           <span class="hours"></span>
           <div class="smalltext">Godzin</div>
         </div>
         <div>
           <span class="minutes"></span>
           <div class="smalltext">Minut</div>
         </div>
         <div>
           <span class="seconds"></span>
           <div class="smalltext">Sekund</div>
         </div>
       </div>

    </td>
    </tr>
    </table>
    </nav>
	</body>
</html>