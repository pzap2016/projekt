<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<head>
		<title>Strona Główna</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>

		<script>
		var logged = "${zal}";
		if(logged){
		    $.notify(
                "Zalogowano pomyślnie.",
            { position:"left top", className: "info" }
            );
        }
        </script>
		<nav>
		    <table>
				<tr>
					<%@ include file="actionButtons.jsp" %>
					<td>
						<h3>Witaj,
							<c:if test="${sessionScope.username != null}">
								${username}<br/>
							</c:if>
							<c:if test="${sessionScope.username == null}">
								nieznajomy
							</c:if>
						</h3>
						<c:if test="${sessionScope.username != null}">
							<center>
								<p id="actionMessage">Dzień dobry!</p>
								<nav style="width:475px;">
									<p align="justify">
										Niniejszy serwis umożliwia przeprowadzanie testów egzaminacyjnych.
										W celu wykonania jakiejś akcji, do której masz uprawnienia,
										posłuż się przyciskami w panelu bocznym.
									</p>
								</nav>
							</center>
						</c:if>
						<c:if test="${sessionScope.username == null}">
							<nav style="width:475px;">
								<p align="justify">
									Niniejszy serwis umożliwia przeprowadzanie testów egzaminacyjnych.
									Jeśli chcesz wziąć udział w którymś z nich - postępuj zgodnie
									z poniższymi instrukcjami.
								</p>
							</nav>
						</c:if>
					</td>
				</tr>
			</table>
			<br/>
			<hr/>
			<c:if test="${sessionScope.username == null}">
				Jeśli posiadasz już konto - kliknij "<b>Zaloguj</b>".<br/>
				Jeśli nie posiadasz, a chcesz - kliknij "<b>Rejestracja</b>".<br/>
			</c:if>
			<c:if test="${sessionScope.username != null}">
				Kliknij "<b>Wyloguj</b>", aby się wylogować.<br/>
			</c:if>
			<br/>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
