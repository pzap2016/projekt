<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<td class="sidebar" style="border-right:2px solid black;">
  <!-- BUTTON 0: "dzień dobry" button -->
  <button id="action0"
      class="actionButton"
      type="button"
      onclick="backToHomePage();">
    Główny
  </button>
  <c:if test="${sessionScope.role == 'Admin'}">
    <br/>
    <!-- BUTTON 1: adminPanel button -->
    <button id="adminPanel"
        class="actionButton"
        type="button"
        onclick="goToAdminPanel();">
      Panel administratora
    </button>
  </c:if>
  <c:if test="${sessionScope.role == 'Edytor'}">
    <br/>
    <!-- BUTTON 2: editorPanel button -->
    <button id="editorPanel"
        class="actionButton"
        type="button"
        onclick="goToTestManager();">
      Panel edytora
    </button>
  </c:if>
  <c:if test="${sessionScope.role != null}">
    <br/>
    <!-- BUTTON 3: userPanel button -->
    <button id="userPanel"
        class="actionButton"
        type="button"
        onclick="goToUserPanel();">
      Panel użytkownika
    </button>
  </c:if>
</td>
