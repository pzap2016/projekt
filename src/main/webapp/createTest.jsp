<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Test - Utwórz</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
    				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                    <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
    			<script>
        		    var logged = "${error}";
        		    if(logged){
        		    $.notify(
                        "Utworzenie testu nie powiodło się",
                    { position:"left top", className: "error" }
                    );
                }
                </script>
  	<nav>
        <table>
          <%@ include file="actionButtons.jsp" %>
          <td>
            <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
            <h2 style="text-align:center;">Tworzenie testu</h2>
          		<form action="CreateTest" method="post">
          			<label for="test_name">Nazwa testu</label><br/>
          			<input id="test_name" type="text" name="test_name" required><br/><br/>
                    <label for="number">Ilość pytań podczas podejścia</label><br/>
                    <input id="number" type="number" name="number" min="1" required><br/><br/>
                    <label for="time">Czas na rozwiązanie testu (min)</label><br/>
                    <input id="time" type="number" name="time" min="1" required><br/>
                <br/>
          			<input type="hidden" id="username" name="username" value="${sessionScope.username}">
          			<button id="save" type="submit">Utwórz</button>
          			<button	id="cancel" type="button" onclick="goToTestManager();">Cofnij</button>
          		</form>
            </c:if>
            <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
                 <%@ include file="accessDenied.jsp" %>
            </c:if>
          </td>
        </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
