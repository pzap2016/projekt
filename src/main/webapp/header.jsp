<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="${pageContext.request.contextPath}/resources/javascript/headerButtons.js"></script>
<p id="headerButtons">
	<c:if test="${sessionScope.username == null}">
		<button id="login" type="button" onclick="login();">Zaloguj</button>
		<button id="register" type="button" onclick="register();">Rejestracja</button>
	</c:if>
	<c:if test="${sessionScope.username != null}">
		<button id="logout" type="button" onclick="logout();">Wyloguj</button>
	</c:if>
</p>
