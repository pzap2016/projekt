<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Panel użytkownika</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
  	<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
      <table>
        <%@ include file="actionButtons.jsp" %>
        <td style="text-align:center;">
          <c:if test="${sessionScope.role != null}">
            <h1>Panel użytkownika</h1>
            <c:if test="${sessionScope.role == 'Admin'}">
              <p>Witaj, adminie!</p>
            </c:if>
            <c:if test="${sessionScope.role == 'Edytor'}">
              <p>Witaj, edytorze!</p>
            </c:if>
            <c:if test="${sessionScope.role == 'User'}">
              <p>Witaj, użytkowniku!</p>
            </c:if>
            <!-- akcje użytkownika -->
   		    <button id="userAction"
                   class="actionButton"
                   type="button"
                   onclick="goToViewTests();">
            Zobacz testy
            </button>
            
            <button id="userAction"
                   class="actionButton"
                   type="button"
                   onclick="goToHistory();">
            Zobacz historię
            </button>
            
			<button id="userAction"
					class="actionButton"
					type="button"
					onclick="editUserData(${sessionScope.idUser});">
			Edytuj profil
			</button>
			
            <button id="userAction"
                    class="actionButton"
                    type="button"
                    onclick="changeUserPassword(${sessionScope.idUser});">
            Zmień hasło
            </button>
          </c:if>
          <c:if test="${sessionScope.role == null}">
               <%@ include file="accessDenied.jsp" %>
          </c:if>
        </td>
      </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
