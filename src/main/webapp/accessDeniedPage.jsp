<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<head>
		<title>Strona Główna</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
		<script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
    <script src="${pageContext.request.contextPath}/resources/javascript/FieldArraysForm.js"></script>
		<nav>
			<table>
				<tr>
					<%@ include file="actionButtons.jsp" %>
					<td>
						<p style="text-align:center;">
						  <b>Nieupoważnionym wstęp wzbroniony!</b><br/>
						  Obawiam się, że nie masz odpowiednich uprawnień,
						  aby oglądać zawartość tej strony.
						</p>
					</td>
				</tr>
			</table>
			<br/>
			<hr/>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
