<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Grupa - Utwórz</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<%@ include file="header.jsp" %>
  <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
	<nav>
    <table>
      <%@ include file="actionButtons.jsp" %>
      <td>
      <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
        <h2 style="text-align:center;">Tworzenie grupy</h2>
    		<form action="groupManager" method="post">
    			<input id="groupName" type="text" name="groupName" required><br/>
          <label>Nazwa grupy</label><br/>
          <br/>
    			<button id="save" type="submit">Utwórz</button><br/>
    		</form>
      </c:if>
      <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Edytor'}">
           <%@ include file="accessDenied.jsp" %>
      </c:if>
      <div style="text-align:center;">
      <button id="cancel"
            type="submit"
            onclick="goToGroupManager();">
          Cofnij
        </button>
      </div>
      </td>
    </table>
	</nav>
	<br/>
	<%@ include file="footer.jsp" %>
</html>
