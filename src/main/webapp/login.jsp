<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<head>
		<title>Zaloguj się</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
	</head>
	<body>
			<script>
    		    var logged = "${error}";
    		    if(logged){
    		    $.notify(
                    "Podano błędne dane logowania",
                { position:"left top", className: "error" }
                );
            }
            </script>
		<p id="headerButtons"></p>
		<br/>
		<nav>
			<c:if test="${sessionScope.role == null}">
				<h2>Zaloguj się!</h2>
				<form action="Login" method="post">
					<input id="text1" type="text" name="user"
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
	            			placeholder="np. jan.kowalski@wp.pl"
							title="Adres e-mail."
							required>
							<br/>
					<label>E-mail</label><br/>
					<input id="text2" type="password" name="passw"
	            			placeholder="********"
	            			required>
							<br/>
					<label>Hasło</label><br/>
					<br/>
					<input id="login" type="submit" value="Zaloguj"><br/>
					<br/>
				</form>
			</c:if>
			<c:if test="${sessionScope.role != null}">
				<p>
					Jesteś już zalogowany/a.
				</p>
			</c:if>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
