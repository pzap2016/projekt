<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Edytuj pytanie</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
  <body>
  	<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
  	<nav>
        <table>
          <%@ include file="actionButtons.jsp" %>
          <td>
            <c:if test="${(sessionScope.role == 'Admin') || (sessionScope.role == 'Edytor')}">
            <h2 style="text-align:center;">Edycja odpowiedzi</h2>
          		<form action="${pageContext.request.contextPath}/Tests/editAnswer" method="post">
          			<label for="answer_Text">Treść odpowiedzi</label><br/>
          			<input id="answer_text" required value="<c:out value="${answerText}"/>" type="text" name="answer_text"><br/><br/><br/>
          			Fałsz<input id="answer_name" type="range" min="0" max="1" value="${answer}" name="answer_name">Prawda<br/>
                <br/>
          			<input type="hidden" id="id" name="id" value="${id}">
          			<input type="hidden" id="idTest" name="idTest" value="${idTest}">
          			<button id="save" type="submit">Edytuj</button>
          			<button	id="cancel" type="button" onclick="window.history.back();">Cofnij</button>
          		</form>
            </c:if>
            <c:if test="${(sessionScope.role != 'Admin') && (sessionScope.role != 'Edytor')}">
                 <%@ include file="accessDenied.jsp" %>
            </c:if>
          </td>
        </table>
  	</nav>
  	<br/>
  	<%@ include file="footer.jsp" %>
  </body>
</html>
