<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<html>
	<head>
		<title>Dodaj odpowiedzi</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
		<link href="resources/css/form.css" rel="stylesheet" type="text/css">
		<script src="resources/javascript/form.js" type="text/javascript"></script>
        <script src="resources/javascript/mainFunctions.js" type="text/javascript"></script>
	</head>
	<body>
	<%@ include file="header.jsp" %>
	    				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/javascript/notify.js"></script>
            		<script>
            		    $.urlParam = function (name) {
                            var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                                          .exec(window.location.href);
                            if(results){
                            return results[1] || 0;
                            }
                            else{
                                return -1;
                            }
                        }
            		    var add = $.urlParam('succesadd');
            		    if(add==="0"){
            		    $.notify(
                            "Pojawił się bład podczas dodawania pytań.",
                            { position:"left top", className: "info" }
                            );
                        }
                    </script>
	<nav>
    <table>
      <%@ include file="actionButtons.jsp" %>
      <td style="text-align:center;">
        <c:if test="${sessionScope.role == 'Admin' || sessionScope.role == 'Edytor'}">
          <h2>Dodaj odpowiedzi dla pytania <br/><c:out value="${question}"/></h2>
          <button id="save" style="width:150px;" onclick="questionFunction()">Dodaj nową </button>
          <div align="center">
            <form action="addAnswers" method="post" id="myForm">
      		    <input type="hidden" name="id_test" value="${idTest}"></input>
                <input type="hidden" name="id_question" value="${idQuestion}"></input>
      			  <br/>
      			  <button id="save" type="submit">Dodaj </button><br/>
      		  </form>
          </div>
        </c:if>
        <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Edytor'}">
             <%@ include file="accessDenied.jsp" %>
        </c:if>
      </td>
    </table>
	</nav>
	<%@ include file="footer.jsp" %>
  </body>
</html>
