<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<html>
	<head>
		<title>Menadżer użytkowników</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mainStyle.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/buttons.css">
	</head>
	<body>
		<%@ include file="header.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/javascript/mainFunctions.js"></script>
		<nav>
      <table>
				<tr>
          <%@ include file="actionButtons.jsp" %>
					<td>
      			<c:if test="${sessionScope.role == 'Admin'}">
              <h2 style="text-align:center;">Użytkownicy</h2>
      				<table class="accTable" align="center">
      					<tr>
      						<th class="accTH">ID</th>
      						<th class="accTH">Imię</th>
                  <th class="accTH">Nazwisko</th>
                  <th class="accTH">Nr indeksu</th>
      						<th class="accTH" colspan="2">Akcje</th>
      					</tr>
      					<c:forEach var="asd" items="${list}">
      						<tr class="accTR">
      							<td class="accTD"><c:out value="${asd.id_User}"/></td>
      							<td class="accTD"><c:out value="${asd.first_name}"/></td>
                    <td class="accTD"><c:out value="${asd.last_name}"/></td>
                    <td class="accTD"><c:out value="${asd.st_index}"/></td>
      							<td class="accTD">
                      <a class="accManager" href="editUser.jsp?idUser=<c:out value="${asd.id_User}"/>" title="Edytuj użytkownika">
                        <img src="${pageContext.request.contextPath}/resources/images/edit2.png"/>
                      </a>
                    </td>
      							<td class="accTD">
                      <a class="accManager" href="removeUser?idUser=<c:out value="${asd.id_User}"/>" title="Usuń użytkownika">
                        <img src="${pageContext.request.contextPath}/resources/images/trash-icon.png"/>
                      </a>
                    </td>
      						</tr>
      					</c:forEach>
      				</table>
              <br/>
              <div style="text-align:center;">
              <button id="cancel"
                    type="submit"
                    onclick="goToAdminPanel();">
                  Cofnij
                </button>
              </div>
            <br/>
      			</c:if>
            <c:if test="${sessionScope.role != 'Admin'}">
      			     <%@ include file="accessDenied.jsp" %>
            </c:if>
					</td>
				</tr>
			</table>
		</nav>
		<br/>
		<%@ include file="footer.jsp" %>
	</body>
</html>
