function backToHomePage(){
    window.location.assign('/PZ2016/index.jsp');
}

function goToAccManager(){
	window.location.assign('/PZ2016/accManager');
}

function goToCreateTest(){
	window.location.assign('/PZ2016/createTest.jsp');
}

function goToCreateGroup(){
	window.location.assign('/PZ2016/createGroup.jsp');
}

function goToHistory(){
	window.location.assign('/PZ2016/Tests/History');
}

function goToViewTests(){
	window.location.assign('/PZ2016/Tests');
}

function goToGroupTests(){
	window.location.assign('/PZ2016/Tests/Groups');
}

function goToViewGroups(){
	window.location.assign('/PZ2016/Groups');
}

function goToGroupManager(){
	window.location.assign('/PZ2016/groupManager');
}

function goToTestManager(){
	window.location.assign('/PZ2016/testManager.jsp');
}

function goToAdminPanel(){
	window.location.assign('/PZ2016/adminPanel.jsp');
}

function goToEditorPanel(){
	window.location.assign('/PZ2016/editorPanel.jsp');
}

function goToUserPanel(){
	window.location.assign('/PZ2016/userPanel.jsp');
}

function startTest(idTest){
	window.location.assign('/PZ2016/Solve?idTest=' + idTest);
}

function editUserPanel(idUser){
	window.location.assign('/PZ2016/editUser.jsp?idUser=' + idUser);
}

function changeUserPassword(idUser){
    window.location.assign('/PZ2016/changeUserPassword.jsp?idUser=' + idUser);
}

function editUserData(idUser){
    window.location.assign('/PZ2016/editUserData.jsp?idUser=' + idUser);
}

function visibility(param) {
    var x = document.getElementsByClassName(param);
    var i;
    for (i = 0; i < x.length; i++){
        if (x[i].style.display === 'none') {
            x[i].style.display = 'table-row';
            x[i].style.backgroundColor = '#EAE7E7';
        } else {
            x[i].style.display = 'none';
        }
    }
}

function toInputText(id){
    if(id.includes("td")){
        placeholder = "Nowe pytanie...";
        x1 = "";
        x2 = "";
    }
    if(id.includes("answer")){
        placeholder = "Nowa odpowiedź...";
        x1 = "<li>";
        x2 = "</li>";
    }
    value = document.getElementById(id).innerHTML;
    field = "<input id='question" + id.slice(2,4) + "' type='text' value='" + value + "' placeholder='" + placeholder + "'></input>";
    button = "<button id='save' onclick='func(" + id + ")'>zapisz</button>";
    button = button.replace(/\s+/g,' ').trim();
    //problem z zagnieżdżaniem w tekście!!!!!!!!!!

    allText = x1 + field + button + x2;
    allText = allText.replace(/\s+/g,' ').trim();
    document.getElementById(id).innerHTML = allText;
}

function func(id){
    var x = "question" + id.id.slice(2,4);
    var x2 = document.getElementById(x).value;
    x2 = x2.replace(/\s+/g,' ').trim();
    $.ajax({
        type: 'POST',
        url: '/PZ2016/addQuestion',
        data: {
            'question': x2
        },
        datatype: 'JSON',
        success: function(data){
            backInTime = "<td id='" + id.id + "' class='accTD, " + x + "' style='text-align:justify; '>";
            backInTime = backInTime + x2 + "</td>";
            //alert(backInTime);
            document.getElementById(id.id).innerHTML = backInTime;
        },
        error: function(){
            alert("Skichało się!");
        },
    });
}
