var i = 0;

function increment(){
    i += 1;
}

function removeElement(parentDiv, childDiv){
    if (childDiv == parentDiv){
        //alert("The parent div cannot be removed.");
    }
    else if (document.getElementById(childDiv)){
        var child = document.getElementById(childDiv);
        var parent = document.getElementById(parentDiv);
        parent.removeChild(child);
    }
    else {
        //alert("Child div has already been removed or does not exist.");
        return false;
    }
}

function questionFunction(){
    var r = document.createElement('span');
    var y = document.createElement("INPUT");
    var qqw = document.createElement("INPUT");
    increment();
    var label = document.createElement("span");
    label.textContent = 'Prawda';
    var label2 = document.createElement("label");
    label2.textContent = 'Fałsz';
    
    qqw.type = 'radio';
    qqw.value = 'true';
    qqw.name = 'trufalse' + i;
    qqw.checked = true;
    var qqw2 = document.createElement("INPUT");
    qqw2.type = 'radio';
    qqw2.value = 'false';
    qqw2.name = 'trufalse' + i;

    y.setAttribute("type", "text");
    y.setAttribute("placeholder", "Odpowiedź");
    y.required = true;
    var g = document.createElement("IMG");
    g.setAttribute("src", "/PZ2016/resources/images/actions-delete-icon.png");

    y.setAttribute("name", "a" + i);
    r.setAttribute("id", "a" + i);
    y.setAttribute("id", "a" + i);
    r.appendChild(y);


    r.appendChild(qqw);
    r.appendChild(label);
    r.appendChild(qqw2);
    r.appendChild(label2);
    g.setAttribute("onclick", "removeElement('myForm','a" + i + "')");
    r.appendChild(g);

    document.getElementById("myForm").appendChild(r);
}

function answerFunction(id){
    var r = document.createElement('span');
    var y = document.createElement("INPUT");
    y.setAttribute("type", "text");
    y.setAttribute("placeholder", "Answer");
    var g = document.createElement("IMG");
    g.setAttribute("src", "/PZ2016/resources/images/actions-delete-icon.png");
    y.setAttribute("Answer", "textelement_" + id);
    y.setAttribute("id","a_" + id);
    r.appendChild(y);
    g.setAttribute("onclick", "removeElement('myForm','a_" + id + "')");
    r.appendChild(g);
    r.setAttribute("id", "a_" + id);
    document.getElementById("myForm").appendChild(r);
}

function addAnswerFunction(id){
    var r = document.createElement('span');
    var el = document.createElement('button');
    el.setAttribute("type", "button");
    el.innerHTML = "+ add answer";
    el.setAttribute("onClick", "answerFunction(" + id + ")");
    r.appendChild(el);
    r.setAttribute("id", id);
    var x = "q_" + id;
    document.getElementById(x).appendChild(r);
}

function resetElements(){
    document.getElementById('myForm').innerHTML = '';
}

function submitJSON(){
    var obj = [];
    var lq = document.getElementById("myForm").querySelectorAll("span > input");

    for(i = 0; i < lq.length; i++) {
        if(lq[i].getAttribute("id").includes("q")){
            j = 0;
            obj.push({
                key: lq[i].getAttribute("id"),
                value: lq[i].value
            });
        } else {
            var ansKey = lq[i].getAttribute("id");
            ansKey = ansKey.concat(".");
            ansKey = ansKey.concat(j)
            obj.push({
                key: ansKey,
                value: lq[i].value
            });
            j++;
        }
    }

    var jsonString = JSON.stringify(obj);
    alert(jsonString);
}
